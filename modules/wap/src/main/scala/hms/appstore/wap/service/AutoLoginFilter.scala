/*
 * (C) Copyright 2010-2014 hSenid Software International (Pvt) Limited.
 *  All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.wap.service

import javax.servlet._
import javax.servlet.http.{HttpServletRequest, HttpServletResponse}

import com.escalatesoft.subcut.inject.{BindingModule, Injectable}
import com.typesafe.scalalogging.slf4j.Logging
import hms.appstore.api.client.{DiscoveryInternalService, DiscoveryService}
import hms.appstore.wap.domain.keyBox
import spray.http.HttpHeader
import spray.http.HttpHeaders.RawHeader

import scala.concurrent.Await
import hms.appstore.api.json.UserDetailsByUsernameReq

class AutoLoginFilter extends Filter with Injectable with WapConfig with Logging {

  val bindingModule: BindingModule = ServiceModule
  val discoveryService = inject[DiscoveryService]
  val discoveryInternalService = inject[DiscoveryInternalService]

  def init(filterConfig: FilterConfig) {
  }

  def destroy() {}

  private def getHeaders(request: ServletRequest): List[HttpHeader] = {
    val httpServletRequest = request.asInstanceOf[HttpServletRequest]
    val headers = (Option(httpServletRequest.getHeader("X-Forwarded-For")), Option(httpServletRequest.getHeader("msisdn")))
    headers match {
      case (Some(h), Some(m)) => List(RawHeader("msisdn", m), RawHeader("X-Forwarded-For", h))
      case _ => List.empty[HttpHeader]
    }
  }

  def doFilter(request: ServletRequest, response: ServletResponse, chain: FilterChain) {

    val httpServletRequest = request.asInstanceOf[HttpServletRequest]
    val httpServletResponse = response.asInstanceOf[HttpServletResponse]

    val session = httpServletRequest.getSession
    val msisdn = httpServletRequest.getHeader("msisdn")
    val rawIp = httpServletRequest.getHeader("X-Forwarded-For")

    val downgraded = session.getAttribute(keyBox.USER_AUTO_LOGIN_REDIRECT_KEY)
    val autoLoginNeg = session.getAttribute(keyBox.USER_AUTO_LOGIN_NEGOTIATED_KEY)
    val transportMode = httpServletRequest.getHeader("X-Transport-Method")

    logger.debug("Transform method : [{}]", transportMode)
    logger.debug("host ip [{}] ", rawIp)
    val filteredHostIp = if (rawIp == null) null
    else if (rawIp.contains(",")) {
      val forwaredeIps = rawIp.split(",")
      forwaredeIps.last.trim
    } else {
      rawIp.trim
    }

    val allowAutoLogin = if (filteredHostIp != null) {
      val result = allowedHosts.collectFirst({
        case r if r.findFirstIn(filteredHostIp).nonEmpty => true
      })
      result.getOrElse(false)
    } else {
      false
    }
    logger.debug("DOWN: " + downgraded)
    logger.debug("granted auto login [{}] for ip [{}]", allowAutoLogin.toString, filteredHostIp)
    if (transportMode != null) {
      if (transportMode.equals("https") && allowAutoLogin && downgraded == null) {
        session.setAttribute(keyBox.USER_AUTO_LOGIN_REDIRECT_KEY, "true")
        val httpRedirectURL = "http://" + redirectDomain + httpServletRequest.getRequestURI
        httpServletResponse.sendRedirect(httpRedirectURL)
        logger.debug("downgrading https => http first-time")
        chain.doFilter(request, response)
        return
      }

      if (transportMode.equals("http") && msisdn != null && autoLoginNeg == null) {

        val httpHeaders = getHeaders(httpServletRequest)
        val authProviders = Await.result(discoveryService.authProviders(platform, httpHeaders), discoveryApiTimeOut)

        logger.debug("auto login the user [{}]", msisdn)

        session.setAttribute(keyBox.USER_AUTO_LOGIN_NEGOTIATED_KEY, "true")
        if (authProviders.preferredProvider.equals("auth-by-msisdn") || authProviders.preferredProvider.equals("auto-signin")) {

          val msisdnAuthResp = Await.result(discoveryService.authenticateByMsisdn(httpHeaders), discoveryApiTimeOut)
          val userDetailsResp = Await.result(discoveryInternalService.userDetailsByMsisdn(msisdn), discoveryApiTimeOut)

          if (msisdnAuthResp.isSuccess) {
            logger.debug("authenticate by msisdn is success [{}]", msisdnAuthResp)
            val sessionId = msisdnAuthResp.sessionId.get
            session.setAttribute(keyBox.USER_AUTH_TOKEN_KEY, sessionId)
            session.setAttribute("sessionId", sessionId)
            session.setAttribute("mobileNo", msisdn)

            if (userDetailsResp.isSuccess) {
              session.setAttribute("username", userDetailsResp.getUserName.getOrElse("user"))
            } else {
              session.setAttribute("username", msisdn)
            }

            val userName = session.getAttribute("username").toString
            val userDetailsByUsernameReq = UserDetailsByUsernameReq(username = userName)
            val fetchUserDetailsRespFuture = discoveryService.userDetailsByUserName(userDetailsByUsernameReq)
            val response = Await.result(fetchUserDetailsRespFuture, discoveryApiTimeOut)
            if (response.isSuccess) {
              session.setAttribute("firstName", response.firstName.get)
              session.setAttribute("lastName", response.lastName.get)
            }

            if (showAutologinFirstNameForUsername) {
              new AutologinUserNameUtil().setUsername(session)
            } else {
              session.setAttribute("display-username", userName)
            }

          } else {
            logger.debug("[{}]", msisdnAuthResp.statusDescription.getOrElse("auto-login user failed."))
          }
        }
        session.setAttribute("AUTO_LOGIN_NEG", "true")
        logger.debug("authProviders [{}]", authProviders)
      }

      if (transportMode.trim.equals("http")) {
        val httpsRedirectURL = "https://" + redirectDomain + httpServletRequest.getRequestURI
        httpServletResponse.sendRedirect(httpsRedirectURL)
        logger.debug("upgrading http => https")
      }
    }

    chain.doFilter(request, response)
  }

}
