/*
 * (C) Copyright 2010-2014 hSenid Software International (Pvt) Limited.
 *  All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.wap.controller

import hms.appstore.api.client.domain.ApplicationView
import hms.appstore.api.client.DiscoveryService
import hms.appstore.wap.service.{WapConfig, ServiceModule}
import hms.appstore.wap.domain.{keyBox, UserDetails}

import org.springframework.ui.ModelMap
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{RequestMethod, RequestMapping}

import java.lang.String
import javax.servlet.http.HttpServletRequest
import scala.concurrent.{ExecutionContext, Await}
import scala.collection.JavaConverters._
import com.escalatesoft.subcut.inject.Injectable
import com.typesafe.scalalogging.slf4j.Logging

@Controller
class MyAppstoreController extends Injectable with WapConfig with Logging {

  val bindingModule = ServiceModule
  private val discoveryService = inject[DiscoveryService]
  private implicit val executionContext = inject[ExecutionContext]


  @RequestMapping(value = Array("/myAppstore"), method = Array(RequestMethod.GET))
  def getMyAppstorePage(request: HttpServletRequest): String = {

    if (UserDetails.apply(request).authenticated) {
      logger.debug("User authenticated")
      "myapps/my-appstore"
    }
    else "login"
  }

  @RequestMapping(value = Array("/myApps"), method = Array(RequestMethod.GET))
  def getMyAppsPage(modelMap: ModelMap, request: HttpServletRequest): String = {

    if (UserDetails.apply(request).authenticated) {
      logger.debug("User authenticated")
      val sessionId = request.getSession.getAttribute(keyBox.USER_AUTH_TOKEN_KEY)
      val mySubscriptionsFuture = discoveryService.mySubscriptions(sessionId.toString)

      val result = for {
        apps <- mySubscriptionsFuture
      } yield apps.getResults.map(ApplicationView(_))

      val mySubscriptions = Await.result(result, discoveryApiTimeOut)
      modelMap.addAttribute("appList", mySubscriptions.asJava)
      "myapps/my-apps"
    } else "login"
  }

  @RequestMapping(value = Array("/myProfile"), method = Array(RequestMethod.GET))
  def getMyProfilePage(modelMap: ModelMap, request: HttpServletRequest): String = {
    "myapps/my-profile"
  }

  @RequestMapping(value = Array("/myDowonloads"), method = Array(RequestMethod.GET))
  def getMyDownloadsPage(modelMap: ModelMap, request: HttpServletRequest): String = {

    val userDetails = UserDetails.apply(request)

    if (userDetails.authenticated) {
      logger.debug("User authenticated")
      val myDownloadsFuture = discoveryService.myDownloads(userDetails.sessionId)

      val result = for {
        apps <- myDownloadsFuture
      } yield apps.getResults.map(ApplicationView(_))

      val myDownloadsResponse = Await.result(result, discoveryApiTimeOut)
      modelMap.addAttribute("myDownloadAppList", myDownloadsResponse.asJava)
      "myapps/my-downloads"
    } else "login"

  }

}
