/*
 * (C) Copyright 2010-2014 hSenid Software International (Pvt) Limited.
 *  All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.wap.domain

import scala.beans.BeanProperty
import javax.servlet.http.HttpServletRequest


class UserDetails {
  @BeanProperty
  var authenticated = false

  @BeanProperty
  var sessionId = ""

  @BeanProperty
  var username = ""

  @BeanProperty
  var mobileNo = ""
}

object UserDetails {
  def apply(req: HttpServletRequest): UserDetails = {
    val userDetails = new UserDetails
    req.getSession.getAttribute(keyBox.USER_AUTH_TOKEN_KEY) match {
      case id: String =>
        userDetails.setSessionId(id)
        userDetails.setAuthenticated(true)
      case _ => userDetails.setAuthenticated(false)
    }

    req.getSession.getAttribute(keyBox.USER_AUTH_USERNAME) match {
      case id: String => userDetails.setUsername(id)
      case _ => userDetails.setUsername("Anonymous")
    }

    req.getSession.getAttribute(keyBox.USER_AUTH_MOBILE_NO) match {
      case id: String => userDetails.setMobileNo(id)
      case _ => userDetails.setMobileNo("")
    }
    userDetails
  }
}

object keyBox {
  val USER_AUTH_TOKEN_KEY = "user.auth.token"
  val USER_AUTH_USERNAME = "user.auth.username"
  val USER_AUTH_MOBILE_NO = "user.auth.mobile.no"
  val USER_AUTO_LOGIN_REDIRECT_KEY = "user.auto.login.redirected"
  val USER_AUTO_LOGIN_NEGOTIATED_KEY = "user.auto.login.negotiated"
}
