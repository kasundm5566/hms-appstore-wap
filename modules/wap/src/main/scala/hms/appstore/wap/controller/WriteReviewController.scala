/*
 * (C) Copyright 2010-2014 hSenid Software International (Pvt) Limited.
 *  All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.wap.controller

import javax.servlet.http.HttpServletRequest

import com.escalatesoft.subcut.inject.Injectable
import com.typesafe.scalalogging.slf4j.Logging
import hms.appstore.api.client.DiscoveryService
import hms.appstore.api.client.util.URLEncoder
import hms.appstore.api.json.UserCommentReq
import hms.appstore.wap.domain.UserDetails
import hms.appstore.wap.service._
import org.springframework.stereotype.Controller
import org.springframework.ui.ModelMap
import org.springframework.web.bind.annotation.{RequestMapping, RequestMethod}
import org.springframework.web.servlet.mvc.support.RedirectAttributes

import scala.concurrent.{Await, ExecutionContext}

@Controller
class WriteReviewController extends Injectable with WapConfig with Logging {

  val bindingModule = ServiceModule
  private val discoveryService = inject[DiscoveryService]
  private implicit val executionContext = inject[ExecutionContext]

  @RequestMapping(value = Array("/write-review"), method = Array(RequestMethod.GET))
  def getWriteReviewForm(modelMap: ModelMap, request: HttpServletRequest, redirectAttributes: RedirectAttributes): String = {
    if((request.getSession.getAttribute("firstName") == null || request.getSession.getAttribute("firstName").equals("")) && checkForUpdatedProfileBeforeUserActions){
      request.getSession.setAttribute("previousPage","/write-review?app-id="+request.getParameter("app-id"))
      redirectAttributes.addFlashAttribute("msg", "update.profile.msg")
      "redirect:/edit-profile"
    }else{
      val appId = request.getParameter("app-id")
      request.getSession.setAttribute("comment-app-id", appId)
      val isEmpty = request.getParameter("isEmpty")
      modelMap.addAttribute("appId", appId)
      if(isEmpty == "true"){
        modelMap.addAttribute("isEmpty", true)
      }
      "review/write-review"
    }
  }

  @RequestMapping(value = Array("/addComment"), method = Array(RequestMethod.POST))
  def addComment(redirectAttributes: RedirectAttributes, request: HttpServletRequest): String = {
    val appId = request.getParameter("app-id")
    if (request.getParameter("review-message").trim != "") {
      val userDetails = UserDetails.apply(request)
      val time = System.currentTimeMillis()
      val abused = Option(request.getParameter("abused"))
      val comment = request.getParameter("review-message")
      val userName = Some("username")
      val sessionId = userDetails.getSessionId
      val reportAbused = if (abused == Some("true")) Some(true) else Some(false)

      val userComment = UserCommentReq(
        appId = appId,
        dateTime = time,
        commentId = None,
        comments = comment,
        username = userName,
        abuse = reportAbused)

      val responseFuture = reportAbused match {
        case Some(true) => discoveryService.reportAbuse(sessionId, userComment.appId, URLEncoder.encode(userComment.comments), Some("wap"))
        case _ => discoveryService.addComment(userComment, sessionId)
      }
      val response = Await.result(responseFuture, discoveryApiTimeOut)
      logger.debug("write review response received {}", response.description)
      AuditLogging.auditLog(request, AuditLogAction.WRITE_REVIEW, AuditLogStatus.SUCCESS)
      redirectAttributes.addAttribute("app-id", appId)
      "redirect:/app-detail"
    } else {
      logger.debug("else comment")
      redirectAttributes.addAttribute("app-id", appId)
      redirectAttributes.addAttribute("isEmpty", "true")
      "redirect:write-review"
    }

  }

}
