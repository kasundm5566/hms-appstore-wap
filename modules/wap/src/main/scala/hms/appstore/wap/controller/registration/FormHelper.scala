package hms.appstore.wap.controller.registration

import java.util
import javax.servlet.http.HttpServletRequest
import com.typesafe.scalalogging.slf4j.Logging
import collection.JavaConversions.enumerationAsScalaIterator
import java.util.Calendar

object FormHelper extends Logging {

  def formatBirthDate(d: String, m: String, y: String) = {

    val builder = new StringBuffer
    val separator = "/"

    builder.append(d)
    builder.append(separator)
    builder.append(m)
    builder.append(separator)
    builder.append(y)

    logger.debug("formatted birth date [{}]", builder.toString)
    builder.toString
  }

  /**
   * @param request
   * @return HTML form parameters as Map(k,v) as named
   */
  def formParametersToMap(request: HttpServletRequest) = {

    val parameterNames = request.getParameterNames.toVector.toList
    val parameters = (for (e <- parameterNames)
    yield (e, request.getParameter(e))).toMap
    logger.debug("form parameters to map [{}] ", parameters)
    parameters
  }

  /**
   *
   * @param request
   * @return tuple (form is valid or not
   *         , form parameter as map (k,v)
   *         , validation message as map (k,v))
   */
  def validateForm(request: HttpServletRequest) = {

    val parameters = formParametersToMap(request)
    val errorMessages = new util.HashMap[String, String]()
    var isValid = true

    def buildValidationMsg(k: String, message: String) {
      errorMessages.put(k, s"${k.replaceAll("[0-9]|_", " ")} $message")
    }

    parameters.keys.foreach(k => {
      val formField = parameters.get(k)
      formField.fold()(v => {
        if (v == "") {
          isValid = false
          buildValidationMsg(k, "cannot be empty")
        }
        if (k == "phone_2" && v.length != 8) {
          isValid = false
          buildValidationMsg(k, "number invalid")
        }
        if (v != "") {
          if (k == "phone_2") {
            if (!v.matches("[0-9]{8}")) {
              buildValidationMsg(k, "number format is not recognized")
              isValid = false
            }
          }
          else if (k == "birth_date") {
            val isNumber = v.matches("[0-9]{1,2}")
            if (!isNumber) {
              buildValidationMsg(k, "invalid")
              isValid = false
            } else if (isNumber && !(0 < Integer.parseInt(v) && Integer.parseInt(v) < 32)) {
              buildValidationMsg(k, "invalid")
              isValid = false
            }
          }
          else if (k == "birth_month") {
            val isNumber = v.matches("[0-9]{1,2}")
            if (!isNumber) {
              buildValidationMsg(k, "invalid")
              isValid = false
            } else if (isNumber && !(0 < Integer.parseInt(v) && Integer.parseInt(v) < 32)) {
              buildValidationMsg(k, "invalid")
              isValid = false
            }
          }
          else if (k == "birth_year") {
            val year = Calendar.getInstance().getWeekYear
            val yearMatch = v.matches("[0-9]{4}")
            if (!yearMatch) {
              buildValidationMsg(k, "invalid")
              isValid = false
            } else if (yearMatch && !(Integer.parseInt(v) < year && Integer.parseInt(v) > 1870)) {
              buildValidationMsg(k, "should be between 1870-" + year)
              isValid = false
            }
          }
          else if (k == "email_address") {
            if ("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$".r.findFirstIn(v) == None) {
              buildValidationMsg(k, "format not recognized")
              isValid = false
            }
          }
          else if (k == "user_name") {
            if (v.length < 5 || v.length > 32) {
              buildValidationMsg(k, "should have 4-32 characters")
              isValid = false
            }
          }
          else if (k == "password") {
            if (v.length < 6) {
              buildValidationMsg(k, "must have minimum length of 6")
              isValid = false
            }
          }
        }
      })
    })

    if (parameters.contains("password") && parameters.contains("password_confirm")) {
      val pwdMatch = parameters.get("password").get.trim().equals(parameters.get("password_confirm").get.trim())
      if (!pwdMatch) {
        errorMessages.put("password", "password mismatch")
        isValid = isValid & pwdMatch
      }
    }
    (isValid, parameters, errorMessages)
  }


}
