/*
/*
 * (C) Copyright 2010-2014 hSenid Software International (Pvt) Limited.
 *  All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.wap.controller

import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation
import org.springframework.web.servlet.ModelAndView

import com.typesafe.scalalogging.slf4j.Logging
import java.util.concurrent.TimeoutException
import spray.client.{UnsuccessfulResponseException, PipelineException}
import org.springframework.web.HttpRequestMethodNotSupportedException

@ControllerAdvice
class ExceptionHandler extends Logging {

  @annotation.ExceptionHandler(Array(classOf[TimeoutException]))
  def handleTimeoutException(ex: Exception) = {
    logger.error(s"TimeoutException Thrown $ex")
    parseErrorMessage("TIMEOUT_EXCEPTION")
  }

  @annotation.ExceptionHandler(Array(classOf[PipelineException], classOf[UnsuccessfulResponseException]))
  def handleCommunicationException(ex: Exception) = {
    logger.error(s"Exceptions Thrown | PipelineException | UnsuccessfulResponseException | $ex")
    parseErrorMessage("COMMUNICATION_EXCEPTION")
  }

  @annotation.ExceptionHandler(Array(classOf[Exception]))
  def handleOtherException(ex: Exception) = {
    logger.error(s"Exception Thrown $ex")
    parseErrorMessage("OTHER_EXCEPTION")
  }

  @annotation.ExceptionHandler(Array(classOf[HttpRequestMethodNotSupportedException]))
  def handleRequestMethodException(ex: Exception) = {
    logger.error(s"Exception Thrown $ex")
    parseErrorMessage("REQUEST_METHOD_EXCEPTION")
  }

  private def parseErrorMessage(message: String): ModelAndView = {
    val modelView = new ModelAndView("error")
    modelView.addObject("message", message)
    modelView
  }
}
*/
