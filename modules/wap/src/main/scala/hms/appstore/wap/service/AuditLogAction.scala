package hms.appstore.wap.service

object AuditLogAction {
  val LOGIN = "login"
  val VIEW_APP = "view-app"
  val USER_REGISTRATION = "user-registration"
  val CHANGE_PASSWORD = "change-password"
  val DOWNLOAD = "download"
  val SUBSCRIBE = "subscribe"
  val UNSUBSCRIBE = "un-subscribe"
  val WRITE_REVIEW = "write-review"
  val RECOVER_ACCOUNT = "recover-account"
}
