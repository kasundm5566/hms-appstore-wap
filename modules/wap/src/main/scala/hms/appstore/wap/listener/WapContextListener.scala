/*
 * (C) Copyright 2010-2014 hSenid Software International (Pvt) Limited.
 *  All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.wap.listener

import hms.appstore.wap.service.WapConfig
import hms.commons.SnmpLogUtil
import javax.servlet.{ServletContextEvent, ServletContextListener}

class WapContextListener extends ServletContextListener with WapConfig {


  def contextInitialized(p1: ServletContextEvent) {
    SnmpLogUtil.log(snmpStartupTrap)
  }

  def contextDestroyed(p1: ServletContextEvent) {
    SnmpLogUtil.log(snmpShutdownTrap)
  }
}
