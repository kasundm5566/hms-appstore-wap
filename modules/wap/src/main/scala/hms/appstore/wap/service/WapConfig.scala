/*
 * (C) Copyright 2010-2014 hSenid Software International (Pvt) Limited.
 *  All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.wap.service

import scala.concurrent.duration._
import scala.collection.JavaConverters._
import com.typesafe.config.ConfigFactory
import scala.util.matching.Regex
import hms.scala.http.util.ConfigUtils

trait WapConfig {

  private val _config = ConfigUtils.prepareSubConfig(ConfigFactory.load(), "appstore.wap")
  
  private val _wapFeatures = ConfigUtils.prepareSubConfig(ConfigFactory.load(), "appstore.wap.features")
  /**
   * The max amount of time we can wait for a discovery-api call.
   * Note: This value should be > spray.can.client.request-timeout in application.conf
   * @return timeout duration
   */
  def discoveryApiTimeOut: Duration = Duration(_config.getString("discovery.api.timeout"))

  def numberOfResultPerPage: Int = _config.getInt("discovery.api.result.per.page")

  def snmpStartupTrap: String = _config.getString("snmp.startup.trap")

  def snmpShutdownTrap: String = _config.getString("snmp.shutdown.trap")

  def imageDirectoryPath: String = "/hms/data/discovery-api/images"

  def operators: Set[String] = _config.getStringList("operator.ids").asScala.toSet

  def redirectDomain:String= _config.getString("redirect.domain")

  def platform:String=_config.getString("platform")


  /**
   * Allowed host ips to enable autoLogin feature
   * @return List of allowed host ips as regex
   */
  def allowedHosts:List[Regex] = _config.getStringList("allowed.hosts").asScala.toList.map(_.r)

  /**
   * Wap Profile Features 
   */
  def preSubscriptionConfirmPageEnabled: Boolean = _wapFeatures.getBoolean("pre.subscription.confirmation.enabled")

  def showAutologinFirstNameForUsername: Boolean = _wapFeatures.getBoolean("autologin.show.firstname.if.username.unavailable")

  def checkForUpdatedProfileBeforeUserActions: Boolean = _wapFeatures.getBoolean("autologin.check.for.updated.profile.before.user.actions")
}
