package hms.appstore.wap.service

import javax.servlet.http.HttpServletRequest

import com.typesafe.scalalogging.slf4j.Logging
import eu.bitwalker.useragentutils.UserAgent
import hms.appstore.wap.domain.UserDetails
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.slf4j.LoggerFactory
import spray.http.HttpHeaders

object AuditLogging extends Logging {
  private lazy val auditLogger = LoggerFactory.getLogger("audit")
  private val fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss,SS")

  def auditLog(httpRequest: HttpServletRequest, action:String, status: String) {

    val userDetails = UserDetails.apply(httpRequest)

    val userAgent: UserAgent = UserAgent.parseUserAgentString(httpRequest.getHeader("User-Agent"))
     //Username|Msisdn|IP|BrowserType|BrowserVersion|SessionId|Module|Action|ActionDescription|TimeStamp|Status
    auditLogger.info("{}|{}|{}|{}|{}|{}|{}|{}|{}|{}|{}"
      , userDetails.getUsername
      , userDetails.getMobileNo
      , httpRequest.getRemoteAddr
      , userAgent.getBrowser.getName
      , userAgent.getBrowserVersion.getMajorVersion
      , userDetails.getSessionId
      , "appstore-wap"
      , action
      , actionDescription.getOrElse(action, "Description not found")
      , fmt.print(new DateTime())
      , status)
  }

  val actionDescription = Map(
    AuditLogAction.LOGIN -> "Login to Appstore-Wap",
    AuditLogAction.VIEW_APP -> "View Application Details",
    AuditLogAction.USER_REGISTRATION -> "User Registration via appstore-wap",
    AuditLogAction.CHANGE_PASSWORD -> "Change user account password Event",
    AuditLogAction.WRITE_REVIEW -> "Write review for application",
    AuditLogAction.SUBSCRIBE -> "Subscribe the application",
    AuditLogAction.UNSUBSCRIBE -> "Un-subscribe the application",
    AuditLogAction.RECOVER_ACCOUNT -> "Recover the user account",
    AuditLogAction.DOWNLOAD -> "Download the application")
}
