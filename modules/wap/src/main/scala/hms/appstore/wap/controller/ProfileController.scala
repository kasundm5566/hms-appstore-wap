/*
 * (C) Copyright 2010-2014 hSenid Software International (Pvt) Limited.
 *  All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.wap.controller

import org.springframework.stereotype.Controller
import com.escalatesoft.subcut.inject.Injectable
import hms.appstore.wap.service.{AutologinUserNameUtil, ServiceModule, WapConfig}
import com.typesafe.scalalogging.slf4j.Logging
import hms.appstore.api.client.DiscoveryService
import hms.appstore.api.json.UserEditReq
import scala.concurrent.Await
import org.springframework.web.bind.annotation.{RequestMethod, RequestMapping}
import javax.servlet.http.{HttpSession, HttpServletRequest}
import org.springframework.ui.Model
import hms.appstore.wap.controller.registration.FormHelper
import java.util
import org.springframework.web.servlet.mvc.support.RedirectAttributes

@Controller
class ProfileController extends Injectable with WapConfig with Logging {

  val bindingModule = ServiceModule
  val discoveryService = inject[DiscoveryService]
  var previousPageBeforeUpdateProfile = ""

  @RequestMapping(value = Array("/edit-profile"), method = Array(RequestMethod.GET))
  def getEditProfilePage( request: HttpServletRequest, session: HttpSession, model: Model): String = {
    previousPageBeforeUpdateProfile=request.getHeader("Referer")
    val formData = new util.HashMap[String, String]()

    formData.put("firstName", session.getAttribute("firstName").toString)
    formData.put("lastName", session.getAttribute("lastName").toString)
    model.addAttribute("formData", formData)
    "edit-profile"
  }

  @RequestMapping(value = Array("/edit-profile"), method = Array(RequestMethod.POST))
  def updateProfileInfo(redirectAttributes: RedirectAttributes, request: HttpServletRequest, model: Model, session: HttpSession): String = {
    val (isValid, formData, errorMsgs) = FormHelper.validateForm(request)

    if (isValid) {

      val fName = formData.get("firstName")
      val lName = formData.get("lastName")
      val uName = request.getSession.getAttribute("username").toString

      val editUserReq = UserEditReq(
        userName = uName,
        //TODO: This parameter (birthDate) should be removed from discovery api.
        birthDate = "08/02/1980",
        firstName = fName,
        lastName = lName)

      val userEditRespFuture = discoveryService.editUser(editUserReq)

      val response = Await.result(userEditRespFuture, discoveryApiTimeOut)
      logger.debug("Profile update response: {}", response.description)

      if (response.isSuccess) {
        session.setAttribute("firstName", fName.get)
        session.setAttribute("lastName", lName.get)
        if(showAutologinFirstNameForUsername){
          new AutologinUserNameUtil().setUsername(session)
        }

        if (session.getAttribute("previousPage") == null || session.getAttribute("previousPage").equals("")) {
          "redirect:" + previousPageBeforeUpdateProfile
        } else {
          val prevPage=session.getAttribute("previousPage")
          session.removeAttribute("previousPage")
          "redirect:" + prevPage
        }
      } else {
        model.addAttribute("formData", loadSavedProfileData(session))
        "edit-profile"
      }
    } else {
      model.addAttribute("formData", loadSavedProfileData(session))
      model.addAttribute("errors", errorMsgs)
      "edit-profile"
    }
  }

  private def loadSavedProfileData(session: HttpSession): util.HashMap[String, String] = {
    val savedData = new util.HashMap[String, String]()
    savedData.put("firstName", session.getAttribute("firstName").toString)
    savedData.put("lastName", session.getAttribute("lastName").toString)
    savedData
  }
}
