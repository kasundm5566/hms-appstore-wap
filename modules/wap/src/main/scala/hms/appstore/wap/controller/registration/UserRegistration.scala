package hms.appstore.wap.controller.registration

import scala.concurrent.Await
import scala.collection.mutable
import org.springframework.ui.Model
import com.typesafe.scalalogging.slf4j.Logging
import scala.collection.convert.decorateAsJava._
import scala.collection.convert.decorateAsScala._
import hms.appstore.api.client.DiscoveryService
import org.springframework.stereotype.Controller
import hms.appstore.wap.service._
import com.escalatesoft.subcut.inject.{BindingModule, Injectable}
import javax.servlet.http.{HttpServletResponse, HttpServletRequest}
import org.springframework.web.bind.annotation.{RequestMethod, RequestMapping}
import hms.appstore.api.json._


@Controller
class UserRegistration extends Logging with Injectable with WapConfig {

  val bindingModule: BindingModule = ServiceModule
  val discoveryService = inject[DiscoveryService]

  @RequestMapping(value = Array("/register"), method = Array(RequestMethod.GET))
  def register(model: Model): String = {
    model.addAttribute("subtitle", "Create Account")
    "register/register-personal"
  }

  @RequestMapping(value = Array("/register"), method = Array(RequestMethod.POST))
  def registerPersonalDetails(request: HttpServletRequest, model: Model): String = {

    val (isValid, formData, errorMsgs) = FormHelper.validateForm(request)
    logger.info("form data [{}]", formData)

    model.addAttribute("formData", formData.asJava)
    model.addAttribute("subtitle", "Create Account")
    if (isValid) {

      val birthDate = FormHelper.formatBirthDate(
        formData.get("birth_date").get
        , formData.get("birth_month").get
        , formData.get("birth_year").get)

      model.addAttribute("birthDateFull", birthDate)
      "register/register-credentials"
    } else {
      model.addAttribute("errors", errorMsgs)
      "register/register-personal"
    }
  }

  @RequestMapping(value = Array("/register-credentials"), method = Array(RequestMethod.POST))
  def registerCredentials(request: HttpServletRequest, model: Model): String = {

    val (isValid, formData, errorMsgs) = FormHelper.validateForm(request)
    logger.info("Form data in register credentials " + formData)

    val s = formData.asJava
    model.addAttribute("formData", s)
    model.addAttribute("subtitle", "Create Account")
    logger.debug("form data [{}]", formData)
    if (isValid) {
      val name = formData.get("fullName").get
      val nameSpitted = if (name.contains(" ")) {
        val spited = name.split(" ")
        (spited(0), spited(1))
      } else {
        (name, "")
      }

      /*val birthDate = FormHelper.formatBirthDate(
        formData.get("birth_date").get
        , formData.get("birth_month").get
        , formData.get("birth_year").get)*/

      val userCreateRespFut = discoveryService.createUser(
        UserRegistrationReq(
          mpin = None,
          msisdn = formData.get("mobileNo").get,
          lastName = Some(nameSpitted._2),
          userName = formData.get("user_name").get,
          operator = operators.head,
          password = "password",
          birthDate = formData.get("birthDateFull").get,
          firstName = Some(nameSpitted._1),
          email = formData.get("emailAddress"),
          verifyMsisdn = true,
          domain = "internal"
        ))

      val userCreateResp = Await.result(userCreateRespFut, discoveryApiTimeOut)
      logger.debug("user create resp [{}]", userCreateResp.toString)
      model.addAttribute("msg", userCreateResp.statusDescription.getOrElse(""))
      if (userCreateResp.isSuccess) {
        AuditLogging.auditLog(request, AuditLogAction.USER_REGISTRATION, AuditLogStatus.SUCCESS)
//        model.addAttribute("mobileNo", buildMobileNum(formData))
        "register/verify"
      } else {
        "register/register-credentials"
      }
    } else {
      AuditLogging.auditLog(request, AuditLogAction.USER_REGISTRATION, AuditLogStatus.FAILED)
      model.addAttribute("errors", errorMsgs)
      "register/register-credentials"
    }
  }

  def buildMobileNum(attributes: Map[String, String]): String = {
    attributes.get("phone_1").get.concat(attributes.get("phone_2").get)
  }

  @RequestMapping(value = Array("/verify"), method = Array(RequestMethod.GET))
  def userVerificationIncomplete(model: Model, req: HttpServletRequest, resp: HttpServletResponse): String = {
    model.addAttribute("subtitle", "Create Account")
    "register/verify"
  }

  @RequestMapping(value = Array("/verify"), method = Array(RequestMethod.POST))
  def userVerification(model: Model, req: HttpServletRequest, resp: HttpServletResponse): String = {

    val formData = FormHelper.formParametersToMap(req)
    logger.debug("formData [{}]", formData)
    val verificationReq: MsisdnVerificationReq = MsisdnVerificationReq(
      username = formData.get("user_name").getOrElse(""),
      verificationCode = formData.get("verificationCode").getOrElse(""))

    val respFuture = discoveryService.verifyMsisdn(verificationReq)
    val resp = Await.result(respFuture, discoveryApiTimeOut)
    logger.debug("verification response [{}]", resp.status.toString)

    model.addAttribute("msg", resp.statusDescription.getOrElse("Success"))
    model.addAttribute("subtitle", "Create Account")
    if (resp.isSuccess) {
      "redirect:login"
    } else {
      model.addAttribute("formData", formData.asJava)
      "register/verify"
    }
  }

  @RequestMapping(value = Array("/editMsisdn"))
  def changeNumber(model: Model, req: HttpServletRequest): String = {

    val formData = FormHelper.formParametersToMap(req)
    val mobileNo = formData.get("mobileNo").getOrElse("")
    val isValidNumber = "[0-9]{10}".r.findFirstIn(mobileNo)
    val isEditView = Option(req.getParameter("view"))

    model.addAttribute("formData", formData.asJava)
    model.addAttribute("subtitle", "Verify your account")

    if (isValidNumber == None || isEditView == None) {
      model.addAttribute("message", "Mobile number is not valid")
      "register/edit-msisdn"
    } else {
      "register/verify"
    }
  }

  @RequestMapping(value = Array("/requestnew"), method = Array(RequestMethod.POST))
  def requestNewCode(model: Model, req: HttpServletRequest): String = {

    val formData = FormHelper.formParametersToMap(req)
    model.addAttribute("formData", formData.asJava)
    model.addAttribute("subtitle", "Create Account")

    val respFuture = discoveryService
      .requestNewVerificationCode(
      MsisdnVerificationCodeReq(
        username = formData.get("user_name").get,
        msisdn = formData.get("mobileNo").get)
    )
    val resp = Await.result(respFuture, discoveryApiTimeOut)
    model.addAttribute("msg", resp.statusDescription.getOrElse(""))
    "register/verify"
  }

  @RequestMapping(value = Array("/forgot-pass"), method = Array(RequestMethod.GET))
  def forgotPassword(model: Model): String = {
    model.addAttribute("subtitle", "Having Trouble signing in?")
    "register/forgot-password"
  }

  @RequestMapping(value = Array("/forgot-pass"), method = Array(RequestMethod.POST))
  def recoverAccount(request: HttpServletRequest, model: Model, response: HttpServletResponse): String = {
    val recoverText = request.getParameter("email")
    model.addAttribute("subtitle", "Having Trouble signing in?")
    val respFuture = discoveryService.recoverUserAccount(UserAccountRecoveryReq(recoverText))
    val resp = Await.result(respFuture, discoveryApiTimeOut)
    model.addAttribute("message", resp.statusDescription.getOrElse(""))
    if (resp.isSuccess) {
      AuditLogging.auditLog(request, AuditLogAction.RECOVER_ACCOUNT, AuditLogStatus.SUCCESS)
      response.setHeader("refresh", "5;login")
    }
    "register/forgot-password"
  }

}