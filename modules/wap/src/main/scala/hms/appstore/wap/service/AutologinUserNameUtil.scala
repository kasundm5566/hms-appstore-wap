package hms.appstore.wap.service

import com.escalatesoft.subcut.inject.Injectable
import com.typesafe.scalalogging.slf4j.Logging
import javax.servlet.http.HttpSession
import hms.appstore.api.client.DiscoveryService
import hms.appstore.api.json.UserDetailsByUsernameReq
import scala.concurrent.Await

/**
 * Created by dinesh on 8/15/17.
 */
class AutologinUserNameUtil extends Injectable with WapConfig with Logging {

  val bindingModule = ServiceModule
  val discoveryService = inject[DiscoveryService]

  def setUsername(session: HttpSession) {

    val userName = session.getAttribute("username").toString
    val userDetailsByUsernameReq = UserDetailsByUsernameReq(username = userName)
    val fetchUserDetailsRespFuture = discoveryService.userDetailsByUserName(userDetailsByUsernameReq)
    val response = Await.result(fetchUserDetailsRespFuture, discoveryApiTimeOut)

    if (response.isSuccess) {
      val username = response.username.get
      val msisdn = response.msisdn.get
      val firstName = response.firstName.get
      logger.debug("UserName {}", username)
      logger.debug("Msisdn {}", msisdn)

      if (username == msisdn) {
        if (firstName == null || firstName == "") {
          session.setAttribute("display-username", msisdn)
        } else {
          session.setAttribute("display-username", firstName)
        }
      } else {
        session.setAttribute("display-username", username)
      }

    }else{
      session.setAttribute("display-username", userName)
    }
    logger.debug("display-username {}", session.getAttribute("display-username"))
  }
}
