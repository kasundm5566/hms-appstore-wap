package hms.appstore.wap.service

object AuditLogStatus {
  val SUCCESS = "success"
  val FAILED = "failed"
}
