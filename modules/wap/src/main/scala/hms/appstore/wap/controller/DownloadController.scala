/*
 * (C) Copyright 2010-2014 hSenid Software International (Pvt) Limited.
 *  All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.wap.controller

import hms.appstore.wap.service._
import hms.appstore.wap.domain.{UserDetails, keyBox}
import hms.appstore.api.client.domain.ApplicationView
import hms.appstore.api.client.DiscoveryService

import org.springframework.ui.ModelMap
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{RequestMethod, RequestMapping}

import java.lang.String
import javax.servlet.http.HttpServletRequest
import scala.concurrent.{ExecutionContext, Await}
import scala.collection.JavaConverters._
import com.escalatesoft.subcut.inject.Injectable
import com.typesafe.scalalogging.slf4j.Logging
import org.springframework.web.servlet.mvc.support.RedirectAttributes

@Controller
class DownloadController extends Injectable with WapConfig with Logging {

  val bindingModule = ServiceModule
  private val discoveryService = inject[DiscoveryService]
  private implicit val executionContext = inject[ExecutionContext]

  @RequestMapping(value = Array("/app-download"), method = Array(RequestMethod.GET))
  def getAppDownloadPage(modelMap: ModelMap, request: HttpServletRequest, redirectAttributes: RedirectAttributes): String = {

    if((request.getSession.getAttribute("firstName") == null || request.getSession.getAttribute("firstName").equals("")) && checkForUpdatedProfileBeforeUserActions){
      request.getSession.setAttribute("previousPage","/app-download?app-id="+request.getParameter("app-id"))
      redirectAttributes.addFlashAttribute("msg", "update.profile.msg")
      "redirect:/edit-profile"
    }else{
      val appId = Option(request.getParameter("app-id")).getOrElse("")
      if (UserDetails.apply(request).authenticated) {

        val sessionId = request.getSession.getAttribute(keyBox.USER_AUTH_TOKEN_KEY)
        val appDetailsFuture = discoveryService.appDetails(sessionId.toString, appId)

        val result = for {
          apps <- appDetailsFuture
        } yield apps

        val appDetailsResp = Await.result(result, discoveryApiTimeOut)
        if (!appDetailsResp.getResult.downloadableBinaries.isEmpty) {
          modelMap.addAttribute("appDetails", ApplicationView(appDetailsResp.getResult))
          "download/app-download"
        } else {
          logger.error("Error occurred while fetching the application details Error[{}]", appDetailsResp.description)
          modelMap.addAttribute("message", "There are no approved downloadable files for this application")
          "error"
        }
      } else {
        modelMap.addAttribute("errorMessage", "Please login to continue")
        modelMap.addAttribute("appId", appId)
        "login"
      }
    }
  }

  @RequestMapping(value = Array("/download-action"), method = Array(RequestMethod.POST))
  def doDownloadAction(modelMap: ModelMap, request: HttpServletRequest): String = {

    if (UserDetails.apply(request).authenticated) {
      val appId = Option(request.getParameter("app-id")).getOrElse("")
      val version = Option(request.getParameter("versions")).getOrElse("")
      val sessionId = request.getSession.getAttribute(keyBox.USER_AUTH_TOKEN_KEY)
      val appDetailsFuture = discoveryService.appDetails(sessionId.toString, appId)

      val result = for {
        apps <- appDetailsFuture
      } yield apps

      val appDetails = Await.result(result, discoveryApiTimeOut)

      if (appDetails.isSuccess) {

        val appDetailsResult = ApplicationView(appDetails.getResult)
        val contentIds = ApplicationView(appDetails.getResult).dnPlatformsAndContentIds.get(version)
        val latestContentId = contentIds.asScala.toList.sortWith(_ > _).head
        val downloadFuture = discoveryService.downloadWithChannel(sessionId.toString, appId, latestContentId, "wap")
        val downloadResponse = Await.result(downloadFuture, discoveryApiTimeOut)

        if (downloadResponse.isSuccess) {
          logger.debug("Download Response Status code [{}]", downloadResponse.statusCode)
          if (downloadResponse.statusCode == "S1005") {
            val piList = downloadResponse.paymentInstrumentList
            val requestId = downloadResponse.downloadRequestId

            val pi = piList.get.map(entry => entry.get("name").getOrElse(""))
            modelMap.addAttribute("appDetails", appDetailsResult)
            modelMap.addAttribute("piList", pi.asJava)
            modelMap.addAttribute("appId", appId)
            modelMap.addAttribute("requestId", requestId.get)

            "download/choose-pi"
          }
          else {
            val wapUrl = downloadResponse.wapUrl.getOrElse("")
            modelMap.addAttribute("wapUrl", wapUrl)
            modelMap.addAttribute("appId", appId)
            AuditLogging.auditLog(request, AuditLogAction.DOWNLOAD, AuditLogStatus.SUCCESS)
            "download/download-success"
          }
        } else {
          modelMap.addAttribute("message", downloadResponse.description)
          logger.error("Error occurred while fetching the application details [{}]", appDetails.description)
          AuditLogging.auditLog(request, AuditLogAction.DOWNLOAD, AuditLogStatus.FAILED)
          "error"
        }
      } else {
        modelMap.addAttribute("message", "Internal server error")
        "error"
      }
    } else {
      "login"
    }
  }

  @RequestMapping(value = Array("/downloadWithCharge"), method = Array(RequestMethod.POST))
  def doDownloadWithCharge(modelMap: ModelMap, request: HttpServletRequest): String = {

    if (UserDetails.apply(request).authenticated) {
      val appId = Option(request.getParameter("app-id")).getOrElse("")
      val requestId = Option(request.getParameter("request-id")).getOrElse("")
      val piName = Option(request.getParameter("pi")).getOrElse("")

      piName match {
        case "M-PAiSA" => {
          modelMap.addAttribute("appId", appId)
          modelMap.addAttribute("requestId", requestId)
          modelMap.addAttribute("pi", piName)
          "download/enter-mpin"
        }
        case _ => {
          val sessionId = request.getSession.getAttribute(keyBox.USER_AUTH_TOKEN_KEY)
          val downloadChargeFuture = discoveryService.downloadWithMobileAccount(sessionId.toString, requestId, piName)
          val dnWithChargeResp = Await.result(downloadChargeFuture, discoveryApiTimeOut)

          if (dnWithChargeResp.isSuccess) {
            modelMap.addAttribute("wapUrl", dnWithChargeResp.wapUrl.getOrElse(""))
            modelMap.addAttribute("appId", appId)
            AuditLogging.auditLog(request, AuditLogAction.DOWNLOAD, AuditLogStatus.SUCCESS)
            "download/download-success"
          } else {
            modelMap.addAttribute("message", dnWithChargeResp.description)
            logger.error("Error occurred while download this application :  [{}]", dnWithChargeResp.description)
            AuditLogging.auditLog(request, AuditLogAction.DOWNLOAD, AuditLogStatus.FAILED)
            "error"
          }
        }
      }
    }
    else "login"
  }

  @RequestMapping(value = Array("/downloadWithMpaisa"), method = Array(RequestMethod.POST))
  def doDownloadWithMpaisa(modelMap: ModelMap, request: HttpServletRequest): String = {

    if (UserDetails.apply(request).authenticated) {
      val appId = Option(request.getParameter("app-id")).getOrElse("")
      val requestId = Option(request.getParameter("request-id")).getOrElse("")
      val piName = Option(request.getParameter("pi")).getOrElse("")
      val mpin = Option(request.getParameter("mpin")).getOrElse("")

      logger.debug("Http Request parameters app-id [{}], request-id [{}], pi [{}] , mpin [{}] ", appId, requestId, piName, mpin)

      val sessionId = request.getSession.getAttribute(keyBox.USER_AUTH_TOKEN_KEY)
      val downloadWithChargeFuture = discoveryService.downloadWithMpaisa(sessionId.toString, requestId, piName, mpin)
      val dnWithChargeResponse = Await.result(downloadWithChargeFuture, discoveryApiTimeOut)

      if (dnWithChargeResponse.isSuccess) {
        val wapUrl = dnWithChargeResponse.wapUrl.getOrElse("")
        modelMap.addAttribute("wapUrl", wapUrl)
        modelMap.addAttribute("appId", appId)
        AuditLogging.auditLog(request, AuditLogAction.DOWNLOAD, AuditLogStatus.SUCCESS)
        "download/download-success"
      } else {
        modelMap.addAttribute("message", dnWithChargeResponse.description)
        logger.error("Error occurred while download this application :  [{}]", dnWithChargeResponse.description)
        AuditLogging.auditLog(request, AuditLogAction.DOWNLOAD, AuditLogStatus.FAILED)
        "error"
      }
    } else {
      "login"
    }
  }
}