/*
 * (C) Copyright 2010-2014 hSenid Software International (Pvt) Limited.
 *  All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.wap.controller

import hms.appstore.wap.service._
import hms.appstore.wap.domain.{UserDetails, keyBox}
import hms.appstore.api.client.DiscoveryService
import hms.appstore.api.client.domain.ApplicationView

import org.springframework.ui.ModelMap
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{RequestMethod, RequestMapping}

import java.lang.String
import javax.servlet.http.HttpServletRequest
import scala.Array
import scala.concurrent.{Await, ExecutionContext}
import com.escalatesoft.subcut.inject.Injectable
import com.typesafe.scalalogging.slf4j.Logging
import org.springframework.web.servlet.mvc.support.RedirectAttributes

@Controller
class SubscriptionController extends Injectable with WapConfig with Logging {

  val bindingModule = ServiceModule
  private val discoveryService = inject[DiscoveryService]
  private implicit val executionContext = inject[ExecutionContext]

  @RequestMapping(value = Array("/subscribeConfirm"), method = Array(RequestMethod.GET))
  def getSubscriptionConfirmPage(modelMap: ModelMap, request: HttpServletRequest, redirectAttributes: RedirectAttributes): String = {

    if((request.getSession.getAttribute("firstName") == null || request.getSession.getAttribute("firstName").equals("")) && checkForUpdatedProfileBeforeUserActions){
      request.getSession.setAttribute("previousPage","/subscribeConfirm?app-id="+request.getParameter("app-id"))
      redirectAttributes.addFlashAttribute("msg", "update.profile.msg")
      "redirect:/edit-profile"
    }else{
      val appId = Option(request.getParameter("app-id")).getOrElse("")
      if (UserDetails.apply(request).authenticated) {
        if(preSubscriptionConfirmPageEnabled){
          val appDetailsFuture = discoveryService.appDetails(appId)
          val result = for {
            appDetailsResult <- appDetailsFuture
          } yield appDetailsResult

          val appDetails = Await.result(result, discoveryApiTimeOut)

          if (appDetails.isSuccess) {
            val appDetailsResult = ApplicationView(appDetails.getResult)
            val chargingLabel = appDetailsResult.chargingLabel
            val nameValuePairs = chargingLabel.split(" ")
            modelMap.addAttribute("appDetails", appDetailsResult)
            modelMap.addAttribute("key", nameValuePairs(0))
            modelMap.addAttribute("app-id", appId)
            "subscribe/subscribe-confirm"
          } else {
            modelMap.addAttribute("message", appDetails.description)
            logger.error("Error occurred while fetching the applications Error[{}]", appDetails.description)
            "error"
          }
        } else {
          subscribe(appId, modelMap, request)
        }
      } else {
        val errorMessage = "Please log in to continue"
        modelMap.addAttribute("errorMessage", errorMessage)
        modelMap.addAttribute("appId", appId)
        "login"
      }
    }
  }

  @RequestMapping(value = Array("/subscribeConfirmSuccess"), method = Array(RequestMethod.POST))
  def getSubscriptionSuccessPage(modelMap: ModelMap, request: HttpServletRequest): String = {

    if (UserDetails.apply(request).authenticated) {
      val appId = Option(request.getParameter("app-id")).getOrElse("")

      subscribe(appId, modelMap, request)
    } else {
      "login"
    }

  }

  @RequestMapping(value = Array("/unSubscribeConfirm"), method = Array(RequestMethod.GET))
  def getUnSubscriptionConfirmPage(modelMap: ModelMap, request: HttpServletRequest, redirectAttributes: RedirectAttributes): String = {

    if((request.getSession.getAttribute("firstName") == null || request.getSession.getAttribute("firstName").equals("")) && checkForUpdatedProfileBeforeUserActions){
      request.getSession.setAttribute("previousPage","/unSubscribeConfirm?app-id="+request.getParameter("app-id"))
      redirectAttributes.addFlashAttribute("msg", "update.profile.msg")
      "redirect:/edit-profile"
    }else{
      val appId = Option(request.getParameter("app-id")).getOrElse("")
      if (UserDetails.apply(request).authenticated) {
        val appDetailsFuture = discoveryService.appDetails(appId)
        val result = for {
          appDetailsResult <- appDetailsFuture
        } yield appDetailsResult

        val appDetails = Await.result(result, discoveryApiTimeOut)

        if (appDetails.isSuccess) {
          val appDetailsResult = ApplicationView(appDetails.getResult)
          val chargingLabel = appDetailsResult.chargingLabel
          val nameValuePairs = chargingLabel.split(" ")
          modelMap.addAttribute("appDetails", appDetailsResult)
          modelMap.addAttribute("key", nameValuePairs(0))
          modelMap.addAttribute("app-id", appId)
          "subscribe/unsubscribe-confirm"
        } else {
          modelMap.addAttribute("message", appDetails.description)
          logger.error("Error occurred while fetching the applications Error[{}]", appDetails.description)
          "error"
        }
      } else {
        val errorMessage = "Please log in to continue"
        modelMap.addAttribute("errorMessage", errorMessage)
        modelMap.addAttribute("appId", appId)
        "login"
      }
    }
  }

    @RequestMapping(value = Array("/unSubscribeConfirmSuccess"), method = Array(RequestMethod.GET))
  def getUnsubscriptionConfirmPage(modelMap: ModelMap, request: HttpServletRequest): String = {

    val appId = Option(request.getParameter("app-id")).getOrElse("")
    if (UserDetails.apply(request).authenticated) {
      val sessionId = request.getSession.getAttribute(keyBox.USER_AUTH_TOKEN_KEY)
      val appDetailsFuture = discoveryService.appDetails(appId)
      val unSubscribeFuture = discoveryService.unSubscribe(sessionId.toString, appId)

      val result = for {
        appDetailsResult <- appDetailsFuture
        unSubscribeResult <- unSubscribeFuture
      } yield (appDetailsResult, unSubscribeResult)

      val (appDetails, unSubscribeResponse) = Await.result(result, discoveryApiTimeOut)
      logger.debug("Application Subscription response [{}] ", appDetails.getResult.subscriptionStatus)

      if (unSubscribeResponse.isSuccess && appDetails.isSuccess) {
        val appDetailsResult = ApplicationView(appDetails.getResult)
        logger.debug("Subscription action response [{}]", unSubscribeResponse.statusDescription)
        AuditLogging.auditLog(request, AuditLogAction.UNSUBSCRIBE, AuditLogStatus.SUCCESS)
        modelMap.addAttribute("appName", appDetailsResult.name)
        modelMap.addAttribute("appId", appDetailsResult.id)
        "subscribe/unsubscribe-success"
      }
      else {
        modelMap.addAttribute("message", unSubscribeResponse.statusDescription.getOrElse("Internal Error Occurred. Try again Later"))
        logger.error("Error occurred while fetching the applications Error[{}]", unSubscribeResponse.description)
        AuditLogging.auditLog(request, AuditLogAction.UNSUBSCRIBE, AuditLogStatus.FAILED)
        "error"
      }
    } else {
      val errorMessage = "Please log in to continue"
      modelMap.addAttribute("errorMessage", errorMessage)
      modelMap.addAttribute("appId", appId)
      "login"
    }
  }
  
  private def subscribe(appId: String, modelMap: ModelMap, request: HttpServletRequest): String = {
    val sessionId = request.getSession.getAttribute(keyBox.USER_AUTH_TOKEN_KEY)
    val appDetailsFuture = discoveryService.appDetails(appId)
    val subscribeFuture = discoveryService.subscribe(sessionId.toString, appId)

    val result = for {
      appDetailsResult <- appDetailsFuture
      subscribeResult <- subscribeFuture
    } yield (appDetailsResult, subscribeResult)

    val (appDetails, subscribeResponse) = Await.result(result, discoveryApiTimeOut)
    logger.debug("Application Subscription response [{}] ", appDetails.getResult.subscriptionStatus)
    logger.debug("Application Success [{}] ", subscribeResponse.status)

    if (subscribeResponse.isSuccess && appDetails.isSuccess) {
      val appDetailsResult = ApplicationView(appDetails.getResult)
      logger.debug("Subscription action response [{}]", subscribeResponse.statusDescription.getOrElse(""))
      AuditLogging.auditLog(request, AuditLogAction.SUBSCRIBE, AuditLogStatus.SUCCESS)
      modelMap.addAttribute("appName", appDetailsResult.name)
      modelMap.addAttribute("appId", appDetailsResult.id)
      modelMap.addAttribute("successMessage", subscribeResponse.statusDescription.getOrElse(""))
      "subscribe/subscribe-success"
    }
    else {
      modelMap.addAttribute("message", subscribeResponse.statusDescription.getOrElse(""))
      AuditLogging.auditLog(request, AuditLogAction.SUBSCRIBE, AuditLogStatus.FAILED)
      logger.error("Error occurred while fetching the applications Error[{}]", subscribeResponse.statusDescription.getOrElse(""))
      "error"
    }
  }
}
