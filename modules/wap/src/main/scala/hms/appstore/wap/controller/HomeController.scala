/*
 * (C) Copyright 2010-2014 hSenid Software International (Pvt) Limited.
 *  All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.wap.controller

import javax.servlet.http.HttpServletRequest

import com.escalatesoft.subcut.inject.Injectable
import com.typesafe.scalalogging.slf4j.Logging
import hms.appstore.api.client.DiscoveryService
import hms.appstore.api.client.domain.ApplicationView
import hms.appstore.wap.service.{ServiceModule, WapConfig}
import org.springframework.stereotype.Controller
import org.springframework.ui.ModelMap
import org.springframework.web.bind.annotation.{RequestMapping, RequestMethod}

import scala.collection.JavaConverters._
import scala.concurrent.{Await, ExecutionContext}

@Controller
class HomeController extends Injectable with WapConfig with Logging {

  val bindingModule = ServiceModule
  private val discoveryService = inject[DiscoveryService]
  private implicit val executionContext = inject[ExecutionContext]

  @RequestMapping(value = Array("", "/", "/index"))
  def getIndexPage :String = {
    "home/index"
  }

  @RequestMapping(value = Array("/newly-added"))
  def getNewlyAddedAppsPage(modelMap: ModelMap, request: HttpServletRequest): String = {

    val limit = numberOfResultPerPage
    val page = Option(request.getParameter("page")).getOrElse("0").toInt
    val start = page * limit

    val appCountFuture = discoveryService.appCount()
    val newlyAddedFuture = discoveryService.newlyAddedApps(start, limit)

    val result = for {
      apps <- newlyAddedFuture
      appCountResult <- appCountFuture
    } yield (apps.getResults.map(ApplicationView(_)), appCountResult)

    val (newlyAddedApps, appCount) = Await.result(result, discoveryApiTimeOut)

    if (appCount.isSuccess) {
      val noOfResults = appCount.getResult.count
      val noOfPages = if ((noOfResults % limit) == 0) noOfResults / limit else (noOfResults / limit) + 1
      modelMap.addAttribute("appList", newlyAddedApps.asJava)
      modelMap.addAttribute("noOfPages", noOfPages)
      modelMap.addAttribute("currentPage", page)
      "home/newly-added"
    } else {
      modelMap.addAttribute("message", appCount.description)
      logger.error("Error occurred while fetching the applications Error[{}]", appCount.description)
      "error"
    }
  }

  @RequestMapping(value = Array("/mostly-used"))
  def getMostlyUsedAppsPage(modelMap: ModelMap, request: HttpServletRequest): String = {

    val limit = numberOfResultPerPage
    val page = Option(request.getParameter("page")).getOrElse("0").toInt
    val start = page * limit

    val appCountFuture = discoveryService.appCount()
    val mostlyUsedFuture = discoveryService.mostlyUsedApps(start, limit)

    val result = for {
      apps <- mostlyUsedFuture
      appCountResult <- appCountFuture
    } yield (apps.getResults.map(ApplicationView(_)), appCountResult)

    val (mostlyUsedApps, appCount) = Await.result(result, discoveryApiTimeOut)

    if (appCount.isSuccess) {
      val noOfResults = appCount.getResult.count
      val noOfPages = if ((noOfResults % limit) == 0) noOfResults / limit else (noOfResults / limit) + 1
      modelMap.addAttribute("appList", mostlyUsedApps.asJava)
      modelMap.addAttribute("noOfPages", noOfPages)
      modelMap.addAttribute("currentPage", page)
      "home/mostly-used"
    } else {
      modelMap.addAttribute("message", appCount.description)
      logger.error("Error occurred while fetching the applications Error[{}]", appCount.description)
      "error"
    }
  }

  @RequestMapping(value = Array("/top-rated"))
  def getTopRatedAppsPage(modelMap: ModelMap, request: HttpServletRequest): String = {

    val limit = numberOfResultPerPage
    val page = Option(request.getParameter("page")).getOrElse("0").toInt
    val start = page * limit

    val appCountFuture = discoveryService.appCount()
    val topRatedFuture = discoveryService.topRatedApps(start, limit)

    val result = for {
      apps <- topRatedFuture
      appCountResult <- appCountFuture
    } yield (apps.getResults.map(ApplicationView(_)), appCountResult)

    val (topRatedApps, appCount) = Await.result(result, discoveryApiTimeOut)

    if (appCount.isSuccess) {

      val noOfResults = appCount.getResult.count
      val noOfPages = if ((noOfResults % limit) == 0) noOfResults / limit else (noOfResults / limit) + 1
      modelMap.addAttribute("appList", topRatedApps.asJava)
      modelMap.addAttribute("noOfPages", noOfPages)
      modelMap.addAttribute("currentPage", page)
      "home/top-rated"
    } else {
      modelMap.addAttribute("message", appCount.description)
      logger.error("Error occurred while fetching the applications Error[{}]", appCount.description)
      "error"
    }
  }

  @RequestMapping(value = Array("/featured-apps"))
  def getFeaturedAppsPage(modelMap: ModelMap, request: HttpServletRequest): String = {

    val limit = numberOfResultPerPage
    val page = Option(request.getParameter("page")).getOrElse("0").toInt
    val start = page * limit

    val appCountFuture = discoveryService.appCount()
    val mostlyUsedFuture = discoveryService.featuredApps(start, limit)

    val result = for {
      apps <- mostlyUsedFuture
      appCountResult <- appCountFuture
    } yield apps.getResults.map(ApplicationView(_))

    val featuredApps = Await.result(result, discoveryApiTimeOut)
    modelMap.addAttribute("appList", featuredApps.asJava)

    "home/featured-apps"
  }
}
