/*
 * (C) Copyright 2010-2014 hSenid Software International (Pvt) Limited.
 *  All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.wap.controller

import javax.servlet.http.HttpServletRequest

import com.escalatesoft.subcut.inject.Injectable
import com.typesafe.scalalogging.slf4j.Logging
import hms.appstore.api.client.DiscoveryService
import hms.appstore.api.client.domain.{ApplicationView, CategoryView}
import hms.appstore.wap.service.{ServiceModule, WapConfig}
import org.springframework.stereotype.Controller
import org.springframework.ui.ModelMap
import org.springframework.web.bind.annotation.{RequestMapping, RequestMethod}

import scala.collection.JavaConverters._
import scala.concurrent.{Await, ExecutionContext}

@Controller
class CategoryController extends Injectable with WapConfig with Logging {

  val bindingModule = ServiceModule
  private val discoveryService = inject[DiscoveryService]
  private implicit val executionContext = inject[ExecutionContext]

  @RequestMapping(value = Array("/categories"), method = Array(RequestMethod.GET))
  def getAppCategories(modelMap: ModelMap, request: HttpServletRequest): String = {

    val limit = numberOfResultPerPage
    val page = Option(request.getParameter("page")).getOrElse("0").toInt
    val noOfPages = getAppCategoriesList.grouped(limit).toList.length
    val getCurrentList = getCurrentPage(page, limit, getAppCategoriesList)

    modelMap.addAttribute("categoryList", getCurrentList.asJava)
    modelMap.addAttribute("noOfPages", noOfPages)
    modelMap.addAttribute("currentPage", page)

    "home/categories"
  }

  def getAppCategoriesList: List[CategoryView] = {
    val appCategoriesFuture = discoveryService.categories()
    val result = for {
      categories <- appCategoriesFuture
    } yield categories.getResults.map(CategoryView(_))
    val appCategories = Await.result(result, discoveryApiTimeOut)
    appCategories

  }

  def getCurrentPage(page: Int, limit: Int, MyList: List[CategoryView]): List[CategoryView] = {

    val AllList = MyList.grouped(limit).toList
    val firstPage = AllList.head
    var tailList = AllList
    var currentPage = firstPage
    if (page == 0)
      currentPage = firstPage
    else {
      for (i <- 1 to page)
        tailList = tailList.tail
      currentPage = tailList.head
    }
    currentPage
  }

  @RequestMapping(value = Array("/categoryApps"), method = Array(RequestMethod.GET))
  def getCategoryAppsPage(modelMap: ModelMap, request: HttpServletRequest): String = {

    val categoryId = Option(request.getParameter("category-id")).getOrElse("")

    val limit = numberOfResultPerPage
    val page = Option(request.getParameter("page")).getOrElse("0").toInt
    val start = page * limit

    val appCountFuture = discoveryService.countByCategory(categoryId)
    val appsByCategoryFuture = discoveryService.appsByCategory(categoryId, start, limit)

    val result = for {
      apps <- appsByCategoryFuture
      appCountResult <- appCountFuture
    } yield (apps.getResults.map(ApplicationView(_)), appCountResult)

    val (appsByCategory, appCount) = Await.result(result, discoveryApiTimeOut)
    if (appCount.isSuccess) {
      val noOfPages = (appCount.getResult.count / limit).toInt

      modelMap.addAttribute("appList", appsByCategory.asJava)
      modelMap.addAttribute("noOfPages", noOfPages)
      modelMap.addAttribute("currentPage", page)
      modelMap.addAttribute("categoryId", categoryId)
      "home/apps-category"
    } else {
      logger.error("Error occurred while fetching the application count Error[{}]", appCount.description)
      modelMap.addAttribute("message", appCount.description)
      "error"
    }

  }
}
