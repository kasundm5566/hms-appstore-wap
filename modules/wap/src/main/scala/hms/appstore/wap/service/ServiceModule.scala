/*
 * (C) Copyright 2010-2014 hSenid Software International (Pvt) Limited.
 *  All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.wap.service

import hms.appstore.api.client.ClientModule

import com.escalatesoft.subcut.inject.NewBindingModule
import concurrent.ExecutionContext
import java.util.concurrent.Executors

object ServiceModule extends NewBindingModule({
  implicit module =>

    module <~ ClientModule

    module.bind[ExecutionContext] toSingle ExecutionContext.
      fromExecutor(Executors.newFixedThreadPool(Runtime.getRuntime.availableProcessors() * 2))
})
