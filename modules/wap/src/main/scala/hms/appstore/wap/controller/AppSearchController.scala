/*
 * (C) Copyright 2010-2014 hSenid Software International (Pvt) Limited.
 *  All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.wap.controller

import hms.appstore.api.client.DiscoveryService
import hms.appstore.api.client.domain.ApplicationView
import hms.appstore.wap.service.{ServiceModule, WapConfig}

import org.springframework.web.bind.annotation.{RequestMethod, RequestMapping}
import org.springframework.stereotype.Controller
import org.springframework.ui.ModelMap

import javax.servlet.http.HttpServletRequest
import java.lang.String
import scala.Array
import scala.collection.JavaConverters._
import scala.concurrent.{ExecutionContext, Await}
import com.escalatesoft.subcut.inject.Injectable
import com.typesafe.scalalogging.slf4j.Logging

@Controller
class AppSearchController extends Injectable with WapConfig with Logging {

  val bindingModule = ServiceModule
  private val discoveryService = inject[DiscoveryService]
  private implicit val executionContext = inject[ExecutionContext]

  @RequestMapping(value = Array("/searchApps"), method = Array(RequestMethod.GET))
  def getSearchedAppsPage(modelMap: ModelMap, request: HttpServletRequest): String = {

    val searchFieldText = Option(request.getParameter("searchField")).getOrElse("")
    val page = Option(request.getParameter("page")).getOrElse("0").toInt
    val limit = numberOfResultPerPage
    val start = page * limit

    if (searchFieldText == "") {
      modelMap.addAttribute("appList", "")
      modelMap.addAttribute("searchFieldText", searchFieldText)
      modelMap.addAttribute("currentPage", page)
      "searched-apps"
    }
    else {
      val totalCountFuture = discoveryService.searchApps(searchFieldText, 0, Int.MaxValue)
      val searchedAppsFuture = discoveryService.searchApps(searchFieldText, start, limit)
      val result = for {
        apps <- searchedAppsFuture
        countResult <- totalCountFuture
      } yield (apps, countResult)

      val (searchedApps, appCount) = Await.result(result, discoveryApiTimeOut)
      if (searchedApps.isSuccess) {
        val noOfResults = appCount.getResults.map(ApplicationView(_)).length
        val noOfPages = if ((noOfResults % limit) == 0) noOfResults / limit else (noOfResults / limit) + 1
        val searchApps = searchedApps.getResults.map(ApplicationView(_))

        modelMap.addAttribute("appList", searchApps.asJava)
        modelMap.addAttribute("noOfPages", noOfPages)
        modelMap.addAttribute("currentPage", page)
        modelMap.addAttribute("searchFieldText", searchFieldText)
        "searched-apps"
      } else {
        modelMap.addAttribute("error", searchedApps.description)
        logger.error("Error occurred while fetching the applications Error[{}]", searchedApps.description)
        "error"
      }
    }
  }
}
