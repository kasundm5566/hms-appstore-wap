/*
 * (C) Copyright 2010-2014 hSenid Software International (Pvt) Limited.
 *  All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.wap.controller

import java.util
import javax.servlet.http.HttpServletRequest

import com.escalatesoft.subcut.inject.Injectable
import com.typesafe.scalalogging.slf4j.Logging
import hms.appstore.api.client.DiscoveryService
import hms.appstore.api.json.{AuthenticateByPasswordReq, PasswordChangeReq}
import hms.appstore.wap.domain.{UserDetails, keyBox}
import hms.appstore.wap.service._
import org.springframework.stereotype.Controller
import org.springframework.ui.ModelMap
import org.springframework.web.bind.annotation.{RequestMapping, RequestMethod}
import org.springframework.web.servlet.mvc.support.RedirectAttributes

import scala.concurrent.{Await, ExecutionContext}

@Controller
class AuthController extends Injectable with WapConfig with Logging {

  val bindingModule = ServiceModule
  private val discoveryService = inject[DiscoveryService]
  private implicit val executionContext = inject[ExecutionContext]

  @RequestMapping(value = Array("/login"), method = Array(RequestMethod.GET))
  def getLoginPage(request: HttpServletRequest): String = {
    if (UserDetails(request).authenticated) "home/index"
    else "login"
  }

  @RequestMapping(value = Array("/logout"), method = Array(RequestMethod.GET))
  def getLogoutPage(modelMap: ModelMap, request: HttpServletRequest): String = {
    request.getSession.invalidate()
    request.setAttribute("logoutSuccess", true)
    "redirect:index"
  }

  @RequestMapping(value = Array("/loginRedirect"), method = Array(RequestMethod.POST))
  def getLoginRedirectPage(modelMap: ModelMap, request: HttpServletRequest, redirectAttr: RedirectAttributes): String = {

    val username = request.getParameter("username")
    val password = request.getParameter("password")
    val appId = request.getParameter("appId")
    logger.info("Appstore wap login attempted by {}  ", username)

    val authenticateFuture = discoveryService.authenticateByPassword(AuthenticateByPasswordReq(username, password))
    val authResponse = Await.result(authenticateFuture, discoveryApiTimeOut)

    if (authResponse.isSuccess) {

      request.getSession.setAttribute(keyBox.USER_AUTH_TOKEN_KEY, authResponse.sessionId.getOrElse(""))
      request.getSession.setAttribute(keyBox.USER_AUTH_USERNAME, username)
      request.getSession.setAttribute(keyBox.USER_AUTH_MOBILE_NO, authResponse.mobileNo.getOrElse(""))
      //TODO: Remove following two lines after fixing the jsp pages
      request.getSession.setAttribute("sessionId", authResponse.sessionId.get)
      request.getSession.setAttribute("username", username)
      AuditLogging.auditLog(request, AuditLogAction.LOGIN, AuditLogStatus.SUCCESS)
      if (appId.isEmpty) "home/index"
      else {
        modelMap.addAttribute("app-id", appId)
        "redirect:app-detail?app-id=" + appId
      }
    } else if (authResponse.statusCode.trim().equals("E5211")) {
      val formData = new util.HashMap[String, String]()
      formData.put("mobileNo", authResponse.mobileNo.getOrElse(""))
      formData.put("user_name", username)
      redirectAttr.addFlashAttribute("formData", formData)
      "redirect:verify"

    } else if (authResponse.statusCode.trim.equals("E5213")) {
      modelMap.addAttribute("username", username)
      "register/change-password"
    } else {
      logger.debug("login attempt failed for the user " + username)
      AuditLogging.auditLog(request, AuditLogAction.LOGIN, AuditLogStatus.FAILED)
      request.setAttribute("error", true)
      "login"
    }
  }

  @RequestMapping(value = Array("/change-password-action"), method = Array(RequestMethod.POST))
  def changePasswordAction(request: HttpServletRequest, modelMap: ModelMap): String = {
    val username = request.getParameter("username")
    val oldPassword = request.getParameter("old-password")
    val newPassword = request.getParameter("new-password")
    val confirmPassword = request.getParameter("confirm-password")
    if (confirmPassword.equals(newPassword)) {
      val req = new PasswordChangeReq(username, newPassword, oldPassword)
      val changePasswordFuture = discoveryService.changePassword(req)
      val changePasswordResponse = Await.result(changePasswordFuture, discoveryApiTimeOut)
      logger.debug("Received change password response from discovery server {}", changePasswordResponse.statusDescription.getOrElse(""))
      if (changePasswordResponse.isSuccess) {
        AuditLogging.auditLog(request, AuditLogAction.CHANGE_PASSWORD, AuditLogStatus.SUCCESS)

        val authenticateFuture = discoveryService.authenticateByPassword(AuthenticateByPasswordReq(username, newPassword))
        val authResponse = Await.result(authenticateFuture, discoveryApiTimeOut)
        if (authResponse.isSuccess) {
          request.getSession.setAttribute(keyBox.USER_AUTH_TOKEN_KEY, authResponse.sessionId.getOrElse(""))
          request.getSession.setAttribute(keyBox.USER_AUTH_USERNAME, username)
          request.getSession.setAttribute(keyBox.USER_AUTH_MOBILE_NO, authResponse.mobileNo.getOrElse(""))
          request.getSession.setAttribute("sessionId", authResponse.sessionId.get)
          request.getSession.setAttribute("username", username)
          AuditLogging.auditLog(request, AuditLogAction.LOGIN, AuditLogStatus.SUCCESS)
          "home/index"
        } else "home/index"
      } else if (changePasswordResponse.statusCode.equals("E5216")) {
        modelMap.addAttribute("username", username)
        modelMap.addAttribute("error", "E5216")
        "register/change-password"
      } else if (changePasswordResponse.statusCode.equals("E5213")) {
        modelMap.addAttribute("username", username)
        modelMap.addAttribute("error", "E5213")
        "register/change-password"
      } else {
        modelMap.addAttribute("username", username)
        "register/change-password"
      }
    } else {
       modelMap.addAttribute("username", username)
       "register/change-password"
    }
  }

}
