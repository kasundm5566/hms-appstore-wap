/*
 * (C) Copyright 2010-2014 hSenid Software International (Pvt) Limited.
 *  All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.wap.controller

import hms.appstore.wap.domain.UserDetails
import hms.appstore.wap.service.{WapConfig, ServiceModule}
import hms.appstore.api.client.DiscoveryService
import hms.appstore.api.client.domain.ApplicationView

import org.springframework.ui.ModelMap
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{RequestMethod, RequestMapping}

import javax.servlet.http.HttpServletRequest
import com.escalatesoft.subcut.inject.Injectable
import com.typesafe.scalalogging.slf4j.Logging
import scala.concurrent.{Await, ExecutionContext}
import scala.collection.JavaConverters._

@Controller
class AppDetailsController extends Injectable with WapConfig with Logging {

  val bindingModule = ServiceModule
  private val discoveryService = inject[DiscoveryService]
  private implicit val executionContext = inject[ExecutionContext]

  @RequestMapping(value = Array("/app-detail"), method = Array(RequestMethod.GET))
  def getAppDetailPage(modelMap: ModelMap, request: HttpServletRequest): String = {

    val appId = request.getParameter("app-id")
    val userDetails = UserDetails.apply(request)

    val RelatedFuture = discoveryService.searchRelated(appId, 0, 3)
    val appDetailsFuture = if (!userDetails.authenticated) {
      discoveryService.appDetails(appId)
    } else {
      discoveryService.appDetails(userDetails.sessionId, appId)
    }

    val result = for {
      relatedApps <- RelatedFuture
      appDetails <- appDetailsFuture
    } yield (relatedApps.getResults.map(ApplicationView(_)), appDetails)

    val (relatedApps, appDetails) = Await.result(result, discoveryApiTimeOut)

    if (appDetails.isSuccess) {
      val appDetailsResult = ApplicationView(appDetails.getResult)
      val chargingLabel = appDetailsResult.chargingLabel
      val nameValuePairs = chargingLabel.split(" ")

      modelMap.addAttribute("appDetails", appDetailsResult)

      modelMap.addAttribute("relatedApps", relatedApps.asJava)
      modelMap.addAttribute("key", nameValuePairs(0))
      "app-details"
    } else {
      modelMap.addAttribute("message", appDetails.description)
      logger.error("Error occurred while fetching the application details [{}]", appDetails.description)
      "error"
    }

  }

}
