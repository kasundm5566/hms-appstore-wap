<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<fmt:bundle basename="messages">
<head>
    <title><fmt:message key="app.download.title"/> </title>
</head>

<body>
<div class="single-container">
    <ul class="app-detail-page-list">
        <li>
            <div class="app-detail-bar">
                <div class="app-details-list-newly">
                    <div class="app-icon">
                        <img src="<c:out value="${appDetails.appIcon}"/>" align="absmiddle"
                             alt="image" class="application-detail-icon"/>
                    </div>
                    <div class="appdetail-detail-app">

                        <div class="application-name-content">
                            <c:set var="fullName" value="${fn:length(appDetails.name)}"/>

                            <c:choose>
                                <c:when test="${fullName > 10}">
                                    <span class="app-name-detail-page-download" title="<c:out value="${appDetails.name}"/>">
                                        <c:out value="${fn:substring(appDetails.name, 0, 10 )}"/>....
                                    </span>
                                </c:when>
                                <c:otherwise>
                                    <span class="app-name-detail-page-download"><c:out value="${appDetails.name}"/></span>
                                </c:otherwise>
                            </c:choose>

                        </div>


                        <div class="application-category-content">
                            <span class="app-name-list"><c:out value="${appDetails.category}"/></span>
                        </div>

                        <div class="rating">
                            <ul class="rating-stars">
                                <c:forEach begin="1" end="5" var="i" >
                                    <c:choose>
                                        <c:when test="${ i < appDetails.rating}">
                                            <li><img src="<c:url value='/resources/img/rating1.png'/>" alt="star"/></li>
                                        </c:when>
                                        <c:otherwise>
                                            <li><img src="<c:url value='/resources/img/rating2.png'/>" alt="star"/></li>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>

    <form action="download-action" method="POST">
        <div class="dropDownMenu-content">
        <input type="text" name="app-id" hidden="true" value="<c:out value="${appDetails.id}"/>" />
            <fieldset class="appdownload-dropdown">
                <label class="appdownload-label"><fmt:message key="app.download.select.platform"/></label>
                <select name="platforms" class="download-by">
                    <%--<option  value=""><fmt:message key="app.download.select.platform"/></option>--%>
                    <c:forEach var="item" items="${appDetails.dnPlatforms}">
                        <option  value="${item.key}">${item.key}</option>
                    </c:forEach>
                </select>
            </fieldset>
            <fieldset class="appdownload-dropdown">
                <label class="appdownload-label"><fmt:message key="app.download.select.version"/></label>
                <select name="versions" class="download-by">
                    <%--<option  value=""><fmt:message key="app.download.select.version"/></option>--%>
                    <c:forEach var="item" items="${appDetails.dnPlatformsAndContentIds}">
                        <option  value="${item.key}">${item.key}</option>
                    </c:forEach>
                </select>
            </fieldset>
        </div>
        <br/>
        <div class="drowpdown-download">
            <div class="enter-wish">
                <input type="submit" value ="Download"  />
            </div>
        </div>
    </form>
</div>
</body>
</fmt:bundle>
</html>