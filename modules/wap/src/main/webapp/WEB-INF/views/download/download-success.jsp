<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<fmt:bundle basename="messages">
<head>
    <title><fmt:message key="app.download.title"/></title>

</head>
<body>
<div class="success-body">
    <div class="success-body-content">
        <c:choose>
            <c:when test="${not empty wapUrl}">
                <p>
                    <fmt:message key="app.download.success.response1"/>
                    <a href="<c:out value="${wapUrl}"/>"><fmt:message key="app.download.success.here"/></a>
                    <fmt:message key="app.download.success.response2"/>
                </p>
            </c:when>
            <c:otherwise>
                <p><fmt:message key="app.download.success.response4"/></p>
            </c:otherwise>
        </c:choose>
    </div>
    <br>
    <div class="enter-wish">
        <form action="#">
            <div class="enter-wish">
                <input type="button" value ="Back" onclick="location.href='app-detail?app-id=<c:out value='${appId}'/>'" />
            </div>
        </form>
    </div>
</div>
</body>
</fmt:bundle>
</html>