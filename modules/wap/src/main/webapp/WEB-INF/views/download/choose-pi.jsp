<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<fmt:bundle basename="messages">
<head>
    <title><fmt:message key="app.download.title"/> </title>
</head>

<body>
<div class="single-container">
    <ul class="app-detail-page-list">
        <li>

            <div class="app-detail-bar">
                <div class="app-details-list-newly">

                    <div class="app-icon">
                        <img src="<c:out value="${appDetails.appIcon}"/>" align="absmiddle"
                             alt="image" class="application-detail-icon"/>
                    </div>


                    <div class="appdetail-detail-app">
                        <div>
                            <div style="height: 22px;">
                                <span class="app-name-detail-page"><c:out value="${appDetails.name}"/></span>

                            </div>
                            <div style="height: 18px;">
                                <span class="app-name-list"><c:out value="${appDetails.category}"/></span>
                            </div>

                            <div >
                                <div class="rating">
                                    <ul class="rating-stars">
                                        <c:forEach begin="1" end="5" var="i" >
                                            <c:choose>
                                                <c:when test="${ i < appDetails.rating}">
                                                    <li><img src="<c:url value='/resources/img/rating1.png'/>" alt="star"/></li>
                                                </c:when>
                                                <c:otherwise>
                                                    <li><img src="<c:url value='/resources/img/rating2.png'/>" alt="star"/></li>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </ul>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            </div>

        </li>
    </ul>

    <form action="downloadWithCharge" method="POST">

    <div class="dropDownMenu-content">

        <input type="text" name="app-id" hidden="true" value="<c:out value="${appId}"/>" />
        <input type="text" name="request-id" hidden="true" value="<c:out value="${requestId}"/>" />

        <fieldset class="appdownload-dropdown">
            <label class="appdownload-label"><fmt:message key="app.download.select.payment.instrument"/></label>
            <select name="pi" class="download-by">
                <c:forEach var="item" items="${piList}">
                    <option  value="${item}" > ${item} </option>
                </c:forEach>
            </select>
        </fieldset>

    </div>
    <div class="drowpdown-download">
        <div class="enter-wish">
            <input type="submit" value ="Download"  />
        </div>
    </div>
    </form>
</div>
</body>
</fmt:bundle>
</html>