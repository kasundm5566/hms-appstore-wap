<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<fmt:bundle basename="messages">
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><fmt:message key="ver.page.title"/></title>
        <link rel="stylesheet" type="text/css" href="resources/css/register/view.css" media="all">
    </head>
    <body id="main_body">
    <div id="form_container">

            <form id="register" class="appnitro register_compact_form" method="post" action="verify">
                <div class="form_description">
                    <h3><fmt:message key="ver.page.form.header"/></h3>
                    <p class="form_feedback"><c:out value="${msg}"/> </p>
                </div>
                <ul>
                    <li id="li_1" class="">
                        <label class="description" for="msisdn"><fmt:message key="ver.page.form.mobileNo"/></label>

                        <div>
                            <input id="msisdn" name="mobileNo" class="element text medium" type="text" maxlength="10"
                                   value='${formData['mobileNo']}' readonly='readonly'>
                        </div>
                    </li>

                    <li id="li_9">
                        <label class="description" for="element_9">
                            <fmt:message key="ver.page.form.verfication.code"/> <span class="required">*</span>
                        </label>

                        <div>
                            <input id="element_9" name="verificationCode" class="element text medium" type="text" maxlength="10" value="">
                        </div>
                    </li>
                    <input type="hidden" name="user_name" value='${formData['user_name']}'/>
                    <li class="buttons">
                        <input id="saveForm" class="button_text" type="submit" name="submit"
                               value='<fmt:message key="ver.page.form.request.create.btn"/>'>
                    </li>
                </ul>
            </form>

            <form id="request" class="appnitro register_compact_form" method="post" action="requestnew">
                <ul>
                    <input type="hidden" name="user_name" value='${formData['user_name']}'/>
                    <input type="hidden" name="mobileNo" value='${formData['mobileNo']}'/>

                    <label class="description inline" for="resend"><fmt:message key="ver.page.form.request.newcode"/></label>
                    <li class="buttons inline">
                        <input id="resend" class="button_text" type="submit" value='<fmt:message key="ver.page.form.request.newcode.btn"/>' name="Request">
                    </li>
                </ul>

            </form>

        <form id="editMsisdn" action="editMsisdn" method="POST" class="appnitro register_compact_form">
            <ul>
                <input type="hidden" name="user_name" value='${formData['user_name']}'/>
                <input type="hidden" name="mobileNo" value='${formData['mobileNo']}'/>

                <label class="description inline" for="resend"><fmt:message key="ver.page.form.edit.msisdn"/></label>
                <li class="buttons inline">
                    <input class="button_text" type="submit" value='<fmt:message key="ver.page.form.request.edit.btn"/>'
                           name="Edit">
                </li>
            </ul>
        </form>

    </div>
    </body>
    </html>
</fmt:bundle>