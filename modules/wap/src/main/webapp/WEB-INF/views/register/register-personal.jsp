<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<fmt:bundle basename="messages">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><fmt:message key="reg.page.title"/></title>
    </head>

    <body id="main_body">
    <div id="form_container">

        <form id="register" class="appnitro register_compact_form" method="post" action="register">
            <div class="form_description">
                <h3><fmt:message key="reg.page.personal.details"/></h3>

                <p class="form_feedback"><c:out value="${msg}"/></p>
            </div>
            <ul>

                <li id="li_1">
                    <label class="description" for="fullName"><fmt:message key="reg.page.form.fullName"/> <span
                            class="required">*</span> </label>

                    <div>
                        <input id="fullName" name="fullName" class="element text medium" type="text" maxlength="100"
                               value='<c:out value="${formData['fullName']}"/>'/>

                        <p class="form_feedback"><c:out value="${errors['fullName']}"/></p>
                    </div>
                </li>

                <li id="li_5">
                    <label class="description" for="element_5_1"><fmt:message key="reg.page.form.mobileNo"/> <span
                            class="required">*</span> </label>
                <span>
                    <input id="element_5_1" name="phone_1" class="element text" size="5" maxlength="5"
                           value="88018" type="text" readonly="readonly"> -
                    <label for="element_5_1">(#####)</label>
                </span>
                <span>
                    <input id="element_5_2" name="phone_2" class="element text" size="8" maxlength="8"
                           value='<c:out value="${formData['phone_2']}"/>' type="text">
                    <label for="element_5_2">(########)</label>

                </span>

                    <p class="form_feedback"><c:out value="${errors['phone_2']}"/></p>

                </li>
                <li id="li_4">
                    <label class="description" for="element_4_1"><fmt:message key="reg.page.form.birthDate"/> <span
                            class="required">*</span> </label>
		<span>
			<input id="element_4_1" name="birth_date" class="element text" size="2" maxlength="2"
                   value='<c:out value="${formData['birth_date']}"/>' type="text"> /
			<label for="element_4_1">DD</label>
		</span>
		<span>
			<input id="element_4_2" name="birth_month" class="element text" size="2" maxlength="2"
                   value='<c:out value="${formData['birth_month']}"/>' type="text"> /
			<label for="element_4_2">MM</label>
		</span>
		<span>
	 		<input id="element_4_3" name="birth_year" class="element text" size="4" maxlength="4"
                   value='<c:out value="${formData['birth_year']}"/>' type="text">
			<label for="element_4_3">YYYY</label>
		</span>

		<span id="calendar_4">
			<img id="cal_img_4" class="datepicker" src="resources/css/register/calendar.gif" alt="Pick a date.">
		</span>
                    <script type="text/javascript">
                        Calendar.setup({
                            inputField: "element_4_3",
                            baseField: "element_4",
                            displayArea: "calendar_4",
                            button: "cal_img_4",
                            ifFormat: "%B %e, %Y",
                            onSelect: selectEuropeDate
                        });
                    </script>

                    <p class="form_feedback"><c:out value="${errors['birth_date']}"/></p>

                    <p class="form_feedback"><c:out value="${errors['birth_month']}"/></p>

                    <p class="form_feedback"><c:out value="${errors['birth_year']}"/></p>


                </li>
                <li id="li_9">
                    <label class="description" for="element_9"><fmt:message key="reg.page.form.emailAddr"/><span
                            class="required">*</span> </label>

                    <div>
                        <input id="element_9" name="email_address" class="element text medium" type="text"
                               maxlength="255"
                               value='<c:out value="${formData['email_address']}"/>'/>

                        <p class="form_feedback"><c:out value="${errors['email_address']}"/></p>
                    </div>
                </li>
                <li class="buttons">
                    <span>
                        <a href="login" class="button_text">
                            <input id="clearForm" class="button_text" type="button" name="submit"
                                   value="<fmt:message key='ver.page.from.btn.cancel'/>"/></a>
                    </span>
                    <span>
                    <input id="saveForm" class="button_text" type="submit" name="submit"
                           value="<fmt:message key='ver.page.from.btn.submit'/>"/>
                    </span>

                </li>
            </ul>
        </form>

    </div>
    </body>
    </html>
</fmt:bundle>