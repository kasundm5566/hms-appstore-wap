<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.2//EN"
"http://www.openmobilealliance.org/tech/DTD/xhtml-mobile12.dtd">
<html xml:lang="en">
<fmt:bundle basename="messages">
    <head>
        <title><fmt:message key="recover.account.page.title"/></title>
    </head>
    <body>

    <div class="content">
        <h4 class="text-center"><fmt:message key="wap.forgot.password.message"/></h4>

        <form method="post" action="forgot-pass">
            <label for='email'><fmt:message key="wap.forgot.password.text.label"/></label>
            <input type='text' name='email' id='email'/>
            <p class="form_feedback">${message}</p>
            <p></p>
            <a href='login' class="button"><fmt:message key="ver.page.from.btn.cancel"/></a>
            <input type='submit' value='<fmt:message key="ver.page.form.btn.request"/>' class='button primary'/>
        </form>
    </div>

    </body>
</fmt:bundle>
</html>
