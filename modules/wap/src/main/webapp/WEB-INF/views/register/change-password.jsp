<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<fmt:bundle basename="messages">
    <head>
        <title><fmt:message key="login.title"/></title>
    </head>

    <body>
    <div class="content-login">
        <h5>Please change your password</h5>
        <div class="login-form">
            <c:if test="${error == 'E5216'}">
                <div class="alert-error">User is currently locked due to invalid password attempts</div>
            </c:if>

            <c:if test="${error == 'E5213'}">
                <div class="alert-error">Password criteria is not fulfilled. Please try again!</div>
            </c:if>

            <form action="change-password-action" method="POST" id="change-password-form">

                <input name="username" value="${username}" hidden="yes" type="hidden"/>

                <fieldset class="username_login-field">
                        <label class="user_login-fields-label"><fmt:message key="app.store.change.old.password"/></label><br/>
                        <input type="password" name="old-password" placeholder=" Enter old password"/>
                </fieldset>

                <fieldset class="username_login-field">
                    <label class="user_login-fields-label"><fmt:message key="app.store.change.new.password"/></label>
                    <input type="password" name="new-password" id="new-password" placeholder="Enter new password"/>
                </fieldset>

                <fieldset class="username_login-field">
                    <label class="user_login-fields-label"><fmt:message key="app.store.change.confirm.password"/></label>
                    <input type="password" name="confirm-password" id="confirm-password" placeholder="Enter new password again"/>
                </fieldset>

                <div class="enter-wish">
                    <input type="submit" value="Submit"/>
                </div>
                <div class="enter-wish">
                    <a href="login" class="register_btn"><fmt:message key="back.button.label"/> </a>
                </div>
            </form>
        </div>
    </div>

    </body>
</fmt:bundle>
</html>

