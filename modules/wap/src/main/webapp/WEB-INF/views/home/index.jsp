<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xml:lang="en">
<fmt:bundle basename="messages">
    <head><title><fmt:message key="page.title"/></title></head>

    <body>
    <%@ include file="/WEB-INF/decorators/searchbar.jsp" %>
    <div class="content">
        <c:if test="${not empty logoutSuccess}">
            <div class="alert-info"><fmt:message key="log.out.success.notification"/></div>
        </c:if>
        <ul class="list-style-one">
            <li>
                <a href="newly-added">
                    <div class="arrow"><img src="<c:url value="/resources/img/arraw.png"/>"/></div>
                    <div class="image">
                        <div class="app-details-list">
                            <div class="app-icon">
                                <img src="<c:url value="/resources/img/newlyaddedApps.png"/>" align="absmiddle"
                                     alt="newlyaddedApps"/>
                            </div>
                            <p class="app-name"><fmt:message key="home.first.category"/></p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="mostly-used">
                    <div class="arrow"><img src="<c:url value="/resources/img/arraw.png"/>"/></div>
                    <div class="image">
                        <div class="app-details-list">
                            <div class="app-icon">
                                <img src="<c:url value="/resources/img/popularApps.png"/>" align="absmiddle"
                                     alt="popularApps"/></div>
                            <p class="app-name"><fmt:message key="home.second.category"/></p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="top-rated">
                    <div class="arrow"><img src="<c:url value="/resources/img/arraw.png"/>"/></div>
                    <div class="image">
                        <div class="app-details-list">
                            <div class="app-icon">
                                <img src="<c:url value="/resources/img/topRatedApps.png"/>" align="absmiddle"
                                     alt="topRatedApps"/></div>
                            <p class="app-name"><fmt:message key="home.third.category"/></p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="featured-apps">
                    <div class="arrow"><img src="<c:url value="/resources/img/arraw.png"/>"/></div>
                    <div class="image">
                        <div class="app-details-list">
                            <div class="app-icon">
                                <img src="<c:url value="/resources/img/featured-apps.png"/>" align="absmiddle"
                                     alt="featuredApps"/></div>
                            <p class="app-name"><fmt:message key="home.fourth.category"/></p>
                        </div>
                    </div>
                </a>
            </li>
            <li><a href="categories">
                <div class="arrow"><img src="<c:url value="/resources/img/arraw.png"/>"/></div>
                <div class="image">
                    <div class="app-details-list">
                        <div class="app-icon">
                            <img src="<c:url value="/resources/img/appCategory.png"/>" align="absmiddle"
                                 alt="appCategory"/></div>
                        <p class="app-name"><fmt:message key="home.fifth.category"/></p>
                    </div>
                </div>
            </a>
            </li>
            <c:if test="${not empty sessionScope.sessionId}">
                <li><a href="myAppstore">
                    <div class="arrow"><img src="<c:url value="/resources/img/arraw.png"/>"/></div>
                    <div class="image">
                        <div class="app-details-list">
                            <div class="app-icon">
                                <img src="<c:url value="/resources/img/pinkheart.png"/>" align="absmiddle" alt="myAppstore"/>
                            </div>
                            <p class="app-name"><fmt:message key="my.app.store.title"/></p>
                        </div>
                    </div>
                </a>
                </li>
            </c:if>
        </ul>
    </div>
    </body>
</fmt:bundle>
</html>
