<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<fmt:bundle basename="messages">
    <head>
        <title>Categories</title>
        <link href="css/template.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
    <%@ include file="/WEB-INF/decorators/searchbar.jsp" %>
    <div class="content">
        <ul class="list-style-appcategory">
            <c:forEach items="${categoryList}" var="category">
                <li>
                    <a href="categoryApps?category-id=<c:out value='${category.id}'/>">
                        <div class="arrow"><img src="/appstore-wap/resources/img/arraw.png" alt="icon"/></div>
                        <div class="image">
                                <div class="app-icon-category">
                                    <img src="/appstore-wap/resources/img/myapplications1.png" align="absmiddle"
                                         alt="image" class="category-icon-scaledown"/>
                                </div>
                                <p class="app-name"><c:out value='${category.id}'/></p>
                        </div>
                    </a>
                </li>
            </c:forEach>
        </ul>
    </div>
    <div class="select-more">
        <div class="select-left">
            <c:if test="${currentPage != 0}">
                <a href="categories?page=${currentPage - 1}"><img src="/appstore-wap/resources/img/prev-button.jpg"
                                                                  alt="image"/></a>
            </c:if>
        </div>
        <div class="select-right">
            <c:if test="${(currentPage +1) lt noOfPages }">
                <a href="categories?page=${currentPage + 1}"><img src="/appstore-wap/resources/img/next-button.jpg"
                                                                  alt="image"/></a>
            </c:if>
        </div>
    </div>
    </body>
</fmt:bundle>
</html>