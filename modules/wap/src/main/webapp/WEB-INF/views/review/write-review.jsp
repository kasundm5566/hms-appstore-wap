<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<fmt:bundle basename="messages">
    <head><title><fmt:message key="app.write.review.title"/></title></head>
    <body>
    <div class="body">
        <div class="single-container">

            <c:if test="${not empty isEmpty}">
                <div class="alert-error">
                    Please enter your comments
                </div>
            </c:if>

            <form action="addComment" role="form" method="POST">
                <div class="dropDownMenu-content">

                    <input type="text" hidden="true" name="app-id" id="app-id" value="<c:out value="${appId}"/>"/>

                    <label class="appdownload-dropdown">Review Type</label>
                    <fieldset class="appdownload-dropdown">
                        <select name="abused"  id="abused" class="download-by">
                            <option value="false">User Comment</option>
                            <option value="true">Report Abuse</option>
                        </select>
                    </fieldset>

                    <fieldset class="appdownload-dropdown">
                        <textarea name="review-message" rows="5" class="review-message"></textarea>
                    </fieldset>

                </div>
                <div class="drowpdown-download">
                    <div class="enter-wish">
                        <input type="submit" value="Submit"/>
                    </div>
                    <div class="enter-wish">
                        <a href="app-detail?app-id=<c:out value='${appId}'/>" class="register_btn">Cancel</a>
                    </div>
                </div>

            </form>
        </div>
    </div>
    </body>
</fmt:bundle>
</html>