<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<fmt:bundle basename="messages">
    <head>
        <title>404 HTTP error</title>
        <link href="<c:url value="/resources/css/template2.css"/>" rel="stylesheet" type="text/css"/>
    </head>
    <body>

    <div class="header">
        <div class="headertop">
            <a href="index">
                <div class="head-devider">
                    <img src="<c:url value="/resources/img/appstore-logo.png"/>" alt="icon" class="homeimage"/>
                </div>
            </a>
            <div class="head-top-right">
                <div class="logos">
                    <c:choose>
                        <c:when test="${not empty sessionScope.sessionId}">
                            <a href="logout"><img src="<c:url value="/resources/img/login.png"/>" alt="Login"/> </a>
                        </c:when>
                        <c:otherwise>
                            <a href="login"><img src="<c:url value="/resources/img/logout.png"/>" alt="Logout"/></a>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
    </div>

    <div class="errorbody">
        <div class="error-body-content">
            <h3><fmt:message key="error.message.for.code.404"/><span style="color: #a6484b;"> (404)</span></h3>
        </div>
    </div>
    <div class="footer">
        <p><fmt:message key="page.footer"/></p>
    </div>
    </body>
</fmt:bundle>
</html>
