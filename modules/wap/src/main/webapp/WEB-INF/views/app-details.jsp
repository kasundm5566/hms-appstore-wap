<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<fmt:bundle basename="messages">
    <head>

        <title><c:out value="${appDetails.name}"/></title>
    </head>
    <body>
    <div class="single-container">
        <span class="app-name-detail-page"><c:out value="${appDetails.name}"/></span>
        <ul class="app-detail-page-list">
            <li>
                <div class="app-detail-bar">
                    <div class="app-details-list-newly">
                        <div class="app-icon">
                            <img src="<c:out value="${appDetails.appIcon}"/>" align="absmiddle"
                                 alt="image" class="application-detail-icon"/>
                        </div>
                        <div class="appdetail-detail-app">
                            <div>
                                <div class="application-name-content">
                                </div>
                                <div class="application-category-content">
                                    <span class="app-name-list"><c:out value="${appDetails.category}"/></span>
                                </div>
                                <div>
                                    <div class="rating">
                                        <ul class="rating-stars">
                                            <c:forEach begin="1" end="5" var="i">
                                                <c:choose>
                                                    <c:when test="${ i <= appDetails.rating}">
                                                        <li><img src="<c:url value='/resources/img/rating1.png'/>"
                                                                 alt="star"/></li>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <li><img src="<c:url value='/resources/img/rating2.png'/>"
                                                                 alt="star"/></li>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:forEach>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="download-section">
                        <c:if test="${ appDetails.canDownload == true}">
                            <div class="appdetail-detail-app">
                                <div>
                                    <div style="width: auto; width: 100%; text-align: center;">
                                        <c:set var="chargingLabel" value="${fn:split(appDetails.chargingLabel, ' ')}"/>
                                        <c:if test="${key =='FREE'}">
                                            <img src="<c:url value='/resources/img/free-app.jpg'/>" alt="freeapp"
                                                 class="charhing-label-icon"/>
                                        </c:if>
                                        <c:if test="${key =='MONTHLY'}">
                                            <img src="<c:url value='/resources/img/monthly-charge.jpg'/>" alt="freeapp"
                                                 class="charhing-label-icon"/>

                                            <div>
                                               <span class="charge-details"
                                                     style="color: #4a8d89; font-size: 9px; height: 10px;">
                                                   <c:out value="${chargingLabel[1]} ${chargingLabel[2]}"/>
                                               </span>
                                            </div>
                                        </c:if>
                                        <c:if test="${key =='DAILY'}">
                                            <img src="<c:url value='/resources/img/daily-charge.jpg'/>" alt="freeapp"
                                                 class="charhing-label-icon"/>

                                            <div>
                                                <span class="charge-details" style="color: #4a8d89; font-size: 9px;">
                                                    <c:out value="${chargingLabel[1]} ${chargingLabel[2]}"/>
                                                </span>
                                            </div>
                                        </c:if>
                                        <c:if test="${key =='PER'}">
                                                <img src="<c:url value='/resources/img/per-usage.jpg'/>" alt="freeapp"
                                                     class="charhing-label-icon"/>
                                                <div>
                                                <span class="charge-details" style="color: #4a8d89; font-size: 9px;">
                                                    <c:out value="${chargingLabel[2]} ${chargingLabel[3]}"/>
                                                </span>
                                                </div>
                                        </c:if>
                                        <a href="app-download?app-id=<c:out value='${appDetails.id}'/>">
                                            <img src="resources/img/download.png" align="absmiddle" alt="image"/>
                                        </a>
                                    </div>
                                </div>
                                <div>
                                    <div style="text-align: center">
                                        <a href="app-download?app-id=<c:out value='${appDetails.id}'/>">
                                            <strong style="color: #4cbb5f; font-size: x-small;">
                                                <fmt:message key="app-detail.download"/></strong>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </c:if>
                        <c:if test="${ appDetails.subscription == true}">
                            <div class="appdetail-detail-app">
                                <div style="width: auto; width: 100%; text-align: center;">
                                    <div style="text-align: center; height:auto ;">
                                        <c:set var="chargingLabel" value="${fn:split(appDetails.chargingLabel, ' ')}"/>
                                        <c:if test="${key =='FREE'}">
                                            <img src="<c:url value='/resources/img/free-app.jpg'/>" alt="freeapp"
                                                 class="charhing-label-icon"/>
                                        </c:if>
                                        <c:if test="${key =='MONTHLY'}">
                                            <img src="<c:url value='/resources/img/monthly-charge.jpg'/>" alt="freeapp"
                                                 class="charhing-label-icon"/>

                                            <div>
                                               <span class="charge-details"
                                                     style="color: #4a8d89; font-size: 9px; height: 10px;">
                                                   <c:out value="${chargingLabel[1]} ${chargingLabel[2]}"/>
                                               </span>
                                            </div>
                                        </c:if>
                                        <c:if test="${key =='DAILY'}">
                                            <img src="<c:url value='/resources/img/daily-charge.jpg'/>" alt="freeapp"
                                                 class="charhing-label-icon"/>

                                            <div>
                                                <span class="charge-details" style="color: #4a8d89; font-size: 9px;">
                                                    <c:out value="${chargingLabel[1]} ${chargingLabel[2]}"/>
                                                </span>
                                            </div>
                                        </c:if>
                                        <c:if test="${key =='PER'}">
                                            <div>
                                                <img src="<c:url value='/resources/img/per-usage.jpg'/>" alt="freeapp"
                                                     class="charhing-label-icon"/>
                                            </div>
                                        </c:if>
                                        <c:if test="${appDetails.subscriptionStatus == 'PENDING CHARGE'}">
                                            <div style="margin-top: 15px">
                                                <label style="background-color: orangered; color: #ffffff; padding: 5px; font-weight: bold ">
                                                    <fmt:message key="app.subscription.status.charge.pending"/>
                                                </label>
                                            </div>
                                        </c:if>

                                    </div>
                                </div>
                                <c:if test="${ appDetails.canUnSubscribe == true }">
                                    <div>
                                        <div style="width: auto; width: 100%; text-align: center;">
                                            <a href="unSubscribeConfirm?app-id=<c:out value='${appDetails.id}'/>"><img
                                                    src="<c:url value="/resources/img/unscribe.png"/>" width=30px
                                                    height=30px align="absmiddle" alt="image"/></a>
                                        </div>
                                    </div>
                                    <div>
                                        <div style="width: auto; width: 100%; text-align: center;">
                                            <a href="unSubscribeConfirm?app-id=<c:out value='${appDetails.id}'/>"><strong
                                                    style="color: #4cbb5f; font-size: x-small;"><fmt:message
                                                    key="app.detail.unsubscribe"/></strong></a>
                                        </div>
                                    </div>
                                </c:if>
                                <c:if test="${ appDetails.canSubscribe == true }">
                                    <div>
                                        <div style="width: auto; width: 100%; text-align: center;">
                                            <a href="subscribeConfirm?app-id=<c:out value='${appDetails.id}'/>"><img
                                                    src="<c:url value="/resources/img/subscribe.png"/>" width=30px
                                                    height=30px align="absmiddle" alt="image"/></a>
                                        </div>
                                    </div>
                                    <div>
                                        <div style="width: auto; width: 100%; text-align: center;">
                                            <a href="subscribeConfirm?app-id=<c:out value='${appDetails.id}'/>"><strong
                                                    style="color: #4cbb5f; font-size: x-small;"><fmt:message
                                                    key="app.detail.subscribe"/></strong></a>
                                        </div>
                                    </div>
                                </c:if>
                            </div>
                        </c:if>
                    </div>
                </div>
            </li>
        </ul>
        <div class="appdetailed-content-bar">
            <span class="app-instruction-header"><fmt:message key="app.details.descrption.header"/></span>

            <p class="app-instruction-content"><c:out value="${appDetails.description}"/></p>

            <span class="app-instruction-header"><fmt:message key="app.details.chargedetails.header"/></span>

            <pre><p class="app-instruction-content"><c:out value='${appDetails.chargingDetails}'/></p></pre>

            <span class="app-instruction-header"><fmt:message key="app.details.instruction.header"/></span>

            <p class="app-instruction-content">
                <c:forEach items="${appDetails.instructions}" var="entry">
                    <c:out value="${entry.key}"/> - <c:out value="${entry.value}"/>
                </c:forEach>
            </p>


            <c:if test="${not empty sessionScope.sessionId}">
                <a id="write-review" class="write-review" href="write-review?app-id=<c:out value='${appDetails.id}'/>">Write
                    Review</a>
            </c:if>

            <span class="app-instruction-header"><fmt:message key="app.details.reviews.header"/></span>

            <p class="app-instruction-content">
                <c:forEach items="${appDetails.userComments}" var="comment">

            <div class="">
                <div class="app-review">
                    <img src="<c:url value="/resources/img/appstore_default_profile.jpg"/>" alt="avatar"
                         class="app-review-profile-pic-area">
                </div>
                <div class="app-view-content">
                    <b><c:out value="${comment.userName}"/></b>
                    <fmt:formatDate value="${comment.date}" pattern="MMMM dd, yyyy"/>
                    <p><c:out value="${comment.comments}"/></p>
                </div>
            </div>
            </c:forEach>
            </p>

            <c:if test="${fn:length(appDetails.screenShots) ge 1}">
            <div class="app-instruction-header">
                <fmt:message key="appstore.app.app.view.page.app.screen.shots.label"/>
            </div>
            <c:forEach items="${appDetails.screenShots}" var="map">
                <c:if test="${!fn:contains(map['url'],'mobile')}">
                    <c:set var="url" value="${map['url']}"/>
                    <span class="#">
                        <img class="screen-shot-view-content"
                             src='<fmt:message key='appstore.app.icon.base.url'/><c:out value="${url}"/>'/>
                    </span>
                </c:if>
            </c:forEach>
        </div>
        </c:if>
    </div>
    </div>

    <div class="related-header">
        <div class="related-bar">
            <span><fmt:message key="app-details.relatedapplications"/></span>
        </div>
    </div>

    <div class="contentrelatedApps">
        <ul class="list-style-appcategory">
            <c:forEach items="${relatedApps}" var="app">
                <li>
                    <a href="app-detail?app-id=<c:out value='${app.id}'/>">
                        <div class="image">
                            <div class="app-details-list-newly">
                                <div class="app-icon">
                                    <img src="<c:out value="${app.appIcon}"/>" align="absmiddle" alt="image"
                                         class="app-icon-scaledown"/>
                                </div>
                                <div class="appdetail">
                                    <div>
                                        <div>
                                            <span class="app-name-newly-added"><c:out value="${app.name}"/></span>
                                        </div>
                                        <div>
                                            <span class="app-name-list"><c:out value="${app.category}"/></span>
                                        </div>
                                    </div>
                                    <div class="rating">
                                        <ul class="rating-stars">
                                            <c:forEach begin="1" end="5" var="i">
                                                <c:choose>
                                                    <c:when test="${ i <= app.rating}">
                                                        <li><img src="<c:url value='/resources/img/rating1.png'/>"
                                                                 alt="star"/></li>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <li><img src="<c:url value='/resources/img/rating2.png'/>"
                                                                 alt="star"/></li>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:forEach>
                                        </ul>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </a>
                </li>
            </c:forEach>
        </ul>

    </div>
    </body>
</fmt:bundle>
</html>