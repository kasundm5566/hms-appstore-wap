<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<fmt:bundle basename="messages">
    <head>
        <title><fmt:message key="searched.result.for"/> <c:out value="${searchFieldText}"/></title>
    </head>

    <body>
    <%@ include file="/WEB-INF/decorators/searchbar.jsp" %>

    <div class="related-header">
        <div class="related-bar">
            <span><fmt:message key="searched.result.for"/> "<span style="color: #9C2AA0;">
                  <c:out value="${searchFieldText}"/>
            </span>"</span>
        </div>
    </div>

    <div class="app-list-content">
        <ul class="list-style-appcategory">
            <c:choose>
                <c:when test="${not empty appList}">
                    <c:forEach items="${appList}" var="app">
                        <li>
                            <a href="app-detail?app-id=<c:out value='${app.id}'/>">
                                <div class="arrow"><img src="/appstore-wap/resources/img/arraw.png" alt="icon"/></div>
                                <div class="image">
                                    <div class="app-details-list-newly">
                                        <div class="app-icon">
                                            <img src="<c:out value="${app.appIcon}"/>" align="absmiddle" alt="image"
                                                 class="app-icon-scaledown"/>
                                        </div>
                                        <div class="appdetail">
                                            <div>
                                                <div>
                                                    <span class="app-name-newly-added"><c:out value="${app.name}"/></span>
                                                </div>
                                                <div>
                                                    <span class="app-name-list"><c:out value="${app.category}"/></span>
                                                </div>
                                            </div>
                                            <div class="rating">
                                                <ul class="rating-stars">
                                                    <c:forEach begin="1" end="5" var="i" >
                                                        <c:choose>
                                                            <c:when test="${ i <= app.rating}">
                                                                <li><img src="<c:url value='/resources/img/rating1.png'/>" alt="star"/></li>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <li><img src="<c:url value='/resources/img/rating2.png'/>" alt="star"/></li>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:forEach>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </c:forEach>
                </c:when>
                <c:otherwise>
                    <div class="related-header">
                        <div class="related-bar">
                            <span><fmt:message key="searched.result.empty"/></span>
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>

        </ul>
    </div>
   <div class="select-more">
        <div class="select-left">
            <c:if test="${currentPage != 0}">
                <a href="searchApps?page=${currentPage - 1}&searchField=${searchFieldText}"><img src="/appstore-wap/resources/img/prev-button.jpg" alt="image"/></a>
            </c:if>
        </div>

        <div class="select-right">
            <c:if test="${(currentPage +1) lt noOfPages }">
                <a href="searchApps?page=${currentPage + 1}&searchField=${searchFieldText}"><img src="/appstore-wap/resources/img/next-button.jpg" alt="image"/></a>
            </c:if>
        </div>
    </div>

    <%--For displaying Page numbers.
    The when condition does not display a link for the current page--%>
  <%--  <table border="1" cellpadding="5" cellspacing="5">
        <tr>
            <c:forEach begin="1" end="${noOfPages}" var="i">
                <c:choose>
                    <c:when test="${currentPage eq i}">
                        <td>${i}</td>
                    </c:when>
                    <c:otherwise>
                        <td><a href="newlyAdded?page=${i}"> ${i} </a></td>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </tr>
    </table>--%>

    </body>
</fmt:bundle>
</html>