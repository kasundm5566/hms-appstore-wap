<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<fmt:bundle basename="messages">
    <head>
        <title><fmt:message key="unsubscribe.success.title"/></title>
    </head>
    <body>
    <div class="success-body">
        <div class="success-body-content">
            <p><fmt:message key="app.unsubscribe.success.response"/> <c:out value="${appName}"/><span>
        </span></p>
        </div>
        <br>
        <div class="">
            <form action="app-detail" class="enter-wish">
                <div class="enter-wish">
                    <input type="text" name="app-id" value="<c:out value="${appId}"/>" hidden="true"/>
                    <input type="submit" value ="Back" />
                </div>
            </form>
        </div>
    </div>
    </body>
</fmt:bundle>
</html>