<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<fmt:bundle basename="messages">
<head>
    <title><fmt:message key="subscribe.page.title"/></title>
</head>

<body>
<div class="body">
    <form action="subscribeConfirmSuccess?app-id=<c:out value="${appDetails.id}"/>" method="POST">
    <div class="single-container">
        <ul class="app-detail-page-list">
            <li>
                <input type="text" name="app-id" value="<c:out value="${appDetails.id}"/>" hidden="true">
                <div class="app-detail-bar">
                    <div class="app-details-list-newly">
                        <div class="app-icon">
                            <img src="<c:out value="${appDetails.appIcon}"/>" align="absmiddle"
                                 alt="image" class="application-detail-icon"/>
                        </div>
                        <div class="appdetail-detail-app">
                            <div>
                                <div class="application-name-content">
                                    <span class="app-name-detail-page-download"><c:out value="${appDetails.name}"/></span>
                                </div>
                                <div class="application-category-content">
                                    <span class="app-name-list"><c:out value="${appDetails.category}"/></span>
                                </div>

                                <div >
                                    <div class="rating">
                                        <ul class="rating-stars">
                                            <c:forEach begin="1" end="5" var="i" >
                                                <c:choose>
                                                    <c:when test="${ i < appDetails.rating}">
                                                        <li><img src="<c:url value='/resources/img/rating1.png'/>" alt="star"/></li>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <li><img src="<c:url value='/resources/img/rating2.png'/>" alt="star"/></li>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:forEach>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
        <div class="appdetailed-content-bar">

            <p class="app-instruction-content"><fmt:message key="subscribe.you.will.be.charged"/> <c:out value="${appDetails.chargingLabel}"/>,
                <fmt:message key="subscribe.for.the.application"/> <c:out value="${appDetails.name}"/>.
                <fmt:message key="subscribe.do.you.want.to.subscribe"/>
            </p>

        </div>
    </div>
        <div class="">
            <div class="subscribeleft">

                <div class="enter-wish">
                    <input type="submit" value ="Confirm"  />
                </div>
                    <div class="enter-wish">
                        <input type="button" value ="Cancel" onclick="location.href='app-detail?app-id=<c:out value='${appDetails.id}'/>'" />
                    </div>

            </div>
        </div>
    </form>
    </div>


</body>
</fmt:bundle>
</html>