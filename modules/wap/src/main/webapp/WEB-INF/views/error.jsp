<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<fmt:bundle basename="messages">
    <head>
        <title><fmt:message key="page.title"/></title>
    </head>
    <body>
    <div class="errorbody">
        <div class="error-body-content">
            <h2><fmt:message key="exception.header"/></h2>

            <h3>
                <c:choose>
                    <c:when test="${message=='COMMUNICATION_EXCEPTION'}">
                        <fmt:message key="exception.communication"/>
                    </c:when>
                    <c:when test="${message=='TIMEOUT_EXCEPTION'}">
                        <fmt:message key="exception.timeout"/>
                    </c:when>
                    <c:when test="${message=='REQUEST_METHOD_EXCEPTION'}">
                        <fmt:message key="exception.request.method"/>
                    </c:when>
                    <c:when test="${message=='OTHER_EXCEPTION'}">
                        <fmt:message key="exception.internal"/>
                    </c:when>
                    <c:otherwise>
                        <c:out value="${message}"/>
                    </c:otherwise>
                </c:choose>
            </h3>
        </div>
    </div>
    </body>
</fmt:bundle>
</html>