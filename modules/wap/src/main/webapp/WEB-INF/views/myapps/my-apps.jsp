<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<fmt:bundle basename="messages">
    <head><title><fmt:message key="my.app.store.myapplications"/></title></head>

    <body>
    <%@ include file="/WEB-INF/decorators/searchbar.jsp" %>
    <div class="app-list-content">
        <ul class="list-style-appcategory">
            <c:if test="${empty appList}">
                <div class="application-empty">
                    <span><fmt:message key="app.result.empty"/></span>
                </div>
                <p><a href="myAppstore" class="empty-back"><u><fmt:message key="back.button.label"/></u></a></p>
            </c:if>
            <c:forEach items="${appList}" var="app">
                <li>
                    <a href="app-detail?app-id=<c:out value='${app.id}'/>">
                        <div class="arrow"><img src="<c:url value="/resources/img/arraw.png"/>" alt="icon"/></div>

                        <div class="appdetail-downloadapps">
                            <p>Subscribed Date</p>
                            <p><format:formatDate value="${app.requestedDate}" pattern="MMM dd YYYY"/></p>
                            <p><c:out value="${app.subscriptionStatus}"/></p>
                        </div>
                        <div class="image">
                            <div class="app-details-list-newly">
                                <div class="app-icon">
                                    <img src="<c:out value="${app.appIcon}"/>" align="absmiddle" alt="image"
                                         class="app-icon-scaledown"/>
                                </div>
                                <div class="appdetail">
                                    <div>
                                        <div>
                                            <span class="app-name-newly-added"><c:out value="${app.name}"/></span>
                                        </div>
                                        <div>
                                            <span class="app-name-list"><c:out value="${app.category}"/></span>
                                        </div>
                                    </div>
                                    <div class="rating">
                                        <ul class="rating-stars">
                                            <c:forEach begin="1" end="5" var="i">
                                                <c:choose>
                                                    <c:when test="${ i < appDetails.rating}">
                                                        <li><img src="<c:url value='/resources/img/rating1.png'/>"
                                                                 alt="star"/></li>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <li><img src="<c:url value='/resources/img/rating2.png'/>"
                                                                 alt="star"/></li>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:forEach>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
            </c:forEach>
        </ul>
    </div>
    </body>
</fmt:bundle>
</html>