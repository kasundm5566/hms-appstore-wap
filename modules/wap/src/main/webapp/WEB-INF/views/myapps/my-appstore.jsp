<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<fmt:bundle basename="messages">

<head>
    <title><fmt:message key="my.app.store.title"/></title>
</head>

<body>
<%@ include file="/WEB-INF/decorators/searchbar.jsp" %>

    <div class="content">
        <ul class="list-style-one">

            <c:if test="${not empty sessionScope.sessionId}">
                <li><a href="myApps">
                    <div class="arrow"><img src="<c:url value="/resources/img/arraw.png"/>" alt="icon"/></div>
                    <div class="image">
                        <div class="app-details-list">
                                <div class="app-icon"><img src="<c:url value="/resources/img/myapplications.png"/>" align="absmiddle" alt="MyApps" /></div>
                                <p class="app-name"><fmt:message key="my.app.store.myapplications"/></p>
                        </div>
                    </div></a>
                </li>
                <li><a href="myDowonloads">
                    <div class="arrow"><img src="/appstore-wap/resources/img/arraw.png" alt="icon"/></div>
                    <div class="image">
                        <div class="app-details-list">
                                <div class="app-icon"><img src="/appstore-wap/resources/img/newdownload.png" align="absmiddle" alt="image" /></div>
                                <p class="app-name"><fmt:message key="my.app.store.downloads"/></p>
                        </div>
                    </div></a>
                </li>
            </c:if>
        </ul>
    </div>
</body>
</fmt:bundle>
</html>