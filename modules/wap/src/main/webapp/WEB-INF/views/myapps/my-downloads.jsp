<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><fmt:message key="mydownloads.title"/></title>
</head>

<body>
<%@ include file="/WEB-INF/decorators/searchbar.jsp" %>

    <div class="content">
        <ul class="liststyleapplist">
            <c:if test="${empty myDownloadAppList}">
                <div class="application-empty">
                    <span><fmt:message key="app.result.empty"/></span>
                </div>
                <p><a href="myAppstore" class="empty-back"><u><fmt:message key="back.button.label"/></u></a></p>
            </c:if>
            <c:forEach items="${myDownloadAppList}" var="info">
                <li>
                    <a href="app-detail?app-id=<c:out value='${info.id}'/>">
                    <div class="appdetail-downloadapps">
                        <p class="dateformat"><c:out value="${info.downloadStatus.status}"/></p>
                        <span class="appstatus"><joda:format value="${info.downloadStatus.requestedDate}" pattern="MMM dd yyyy"/></span>
                    </div>
                    <div class="image">

                            <img src="<c:out value="${info.appIcon}"/>" align="absmiddle" alt="image" class="app-icon-scaledown"/>
                            <p class="application-name"><c:out value="${info.name}"/></p>
                    </div>
                    </a>
                </li>
            </c:forEach>

        </ul>

    </div>


</body>
</html>