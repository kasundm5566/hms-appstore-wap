    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
    <!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <fmt:bundle basename="messages">
        <head>
            <title>Database Error</title>

        </head>
        <body>
        <div class="errorbody">
        <div class="application-empty"> ${errorMessage}
        </div>
    </div>
    </body>
</fmt:bundle>
</html>