<div class="login-container-searchbox">
    <form  method="GET"  action="searchApps">
        <fieldset class="search-box-item">
            <div style="width: 100%; float: left;">
                <input id="searchField" name="searchField" type="text" value="${searchFieldText}" placeholder=" <fmt:message key='app.search.label'/>"/>
            </div>
            <div style="float:right; position:relative;">
                <div style="width: 100%;position:absolute; ">
                    <input type="submit"  value="" style="background:url('/appstore-wap/resources/img/search_icon.png') no-repeat;"/>
                </div>
            </div>
        </fieldset>
    </form>
</div>
