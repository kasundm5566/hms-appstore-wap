<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<fmt:bundle basename="messages">
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><fmt:message key="reg.page.title"/></title>
    </head>
    <body id="main_body">
    <div id="form_container">

        <form id="register" class="appnitro register_compact_form" method="post" action="register-credentials">
            <ul>
                <div class="form_description">
                    <h4><fmt:message key="reg.page.create.userpwd"/></h4>
                    <p class="form_feedback"><c:out value="${msg}"/> </p>
                </div>
                <li id="li_6">
                    <label class="description" for="element_6"><fmt:message key="reg.page.form.uname"/> <span
                            class="required">*</span> </label>

                    <div>
                        <input id="element_6" name="user_name" class="element text medium" type="text" maxlength="32"
                               value='${formData["user_name"]}'/>
                        <p class="form_feedback"><c:out value="${errors['user_name']}"/></p>
                    </div>
                </li>
                <li id="li_7">
                    <label class="description" for="element_7"><fmt:message key="reg.page.form.password"/> <span
                            class="required">*</span> </label>

                    <div>
                        <input id="element_7" name="password" class="element text medium" type="password"
                               maxlength="255"
                               value=""/>
                    </div>
                </li>
                <li id="li_8">
                    <label class="description" for="element_8"><fmt:message key="reg.page.form.password.re"/> <span
                            class="required">*</span> </label>

                    <div>
                        <input id="element_8" name="password_confirm" class="element text medium" type="password"
                               maxlength="255"
                               value=""/>
                    </div>
                    <p class="form_feedback"><c:out value="${errors['password']}"/></p>
                </li>
                <%--<div class="form_description">
                    <h3><fmt:message key="reg.page.terms.condt.title"/></h3>
                </div>--%>
                <li></li>
                <p><fmt:message key="reg.page.terms.condt.desc"/></p>
                <li id="li_10">
                </li>

                <input type="hidden" name="fullName" value="${formData['fullName']}"/>
                <input type="hidden" name="birthDateFull" value="${birthDateFull}${formData['birthDateFull']}"/>
                <input type="hidden" name="emailAddress" value="${formData['email_address']}${formData['emailAddress']}"/>
                <input type="hidden" name="mobileNo" value="${formData['phone_1']}${formData['phone_2']}${formData['mobileNo']}"/>

                <li class="buttons">
                    <span>
                     <a href="register" class="previous"><fmt:message key="ver.page.from.btn.back"/></a>
                    </span>
                    <span>
                    <input id="saveForm" class="button_text" type="submit" name="submit"
                           value="<fmt:message key='ver.page.from.btn.submit'/>"/>
                    </span>
                </li>
            </ul>
        </form>
    </div>
    </body>
    </html>
</fmt:bundle>