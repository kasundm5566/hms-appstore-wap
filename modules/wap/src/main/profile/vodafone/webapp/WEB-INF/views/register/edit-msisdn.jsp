<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<fmt:bundle basename="messages">
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><fmt:message key="ver.page.title"/></title>
        <link rel="stylesheet" type="text/css" href="resources/css/register/view.css" media="all">
    </head>
    <body id="main_body">
    <div id="form_container">

        <form id="register" class="appnitro register_compact_form" method="post" action="editMsisdn">
            <div class="form_description">
                <h3><fmt:message key="edit.msisdn.subtitle"/></h3>
                <p class="form_feedback">${message}</p>
            </div>
            <ul>

                <li id="li_1" class="">
                    <label class="description" for="msisdn"><fmt:message key="ver.page.form.mobileNo"/></label>

                    <div>
                        <input id="msisdn" name="mobileNo" class="element text medium" type="text" maxlength="10"
                               value='${formData['mobileNo']}' >
                    </div>
                </li>

                <input type="hidden" name="view" value='edit-mobile-num'/>
                <input type="hidden" name="user_name" value='${formData['user_name']}'/>
                <li class="buttons">
                    <input id="saveForm" class="button_text" type="submit" name="submit"
                           value='<fmt:message key="ver.page.from.btn.submit"/>'>
                </li>
            </ul>
        </form>

    </div>
    </body>
    </html>
</fmt:bundle>