<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.2//EN"
"http://www.openmobilealliance.org/tech/DTD/xhtml-mobile12.dtd">
<fmt:bundle basename="messages">

    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <title><sitemesh:write property='title'/></title>
        <link href="<c:url value="/resources/css/template2.css"/>" rel="stylesheet" type="text/css"/>
        <link href="<c:url value="/resources/css/register/view.css"/>" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src='<c:url value="/resources/js/calendar.js"/>'></script>
        <script type="text/javascript" src='<c:url value="/resources/js/view.js"/>'></script>
     </head>
    <body>
    <%@ include file="/WEB-INF/decorators/header-reg.jsp" %>
    <div class="margin-top-25">
    <sitemesh:write property='body'/>
    </div>
    <%@ include file="/WEB-INF/decorators/footer-reg.jsp" %>
    </body>
    </html>

</fmt:bundle>
