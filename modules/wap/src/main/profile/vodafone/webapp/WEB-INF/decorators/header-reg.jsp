<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="header">
    <div class="headertop">
        <a href="index">
            <div class="head-devider">
                <img src="/appstore-wap/resources/img/appstore-logo.png" alt="icon" class="homeimage"/>
            </div>
        </a>

        <div class="head-top-right">
            <div class="logos">
                <c:choose>
                    <c:when test="${not empty sessionScope.sessionId}">
                        <nav>
                            <ul>
                                <li><a href="#"><img src="/appstore-wap/resources/img/settings.png" alt="Login"/></a>
                                    <ul>
                                        <li><a href="#"><img src="/appstore-wap/resources/img/user.png" alt="Login"/>

                                            <div><c:out value="${sessionScope.username}"/></div>
                                        </a></li>
                                        <li><a href="logout"><img src="/appstore-wap/resources/img/login.png"
                                                                  alt="Login"/>

                                            <div>Logout</div>
                                        </a></li>
                                    </ul>
                                </li>

                            </ul>
                        </nav>
                    </c:when>
                    <c:otherwise>
                        <a href="login"><img src="/appstore-wap/resources/img/logout.png" alt="Logout"/></a>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
    <span class="mini-title">${subtitle}</span>
</div>
