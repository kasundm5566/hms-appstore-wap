<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<fmt:bundle basename="messages">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><fmt:message key="reg.page.title"/></title>
    <link rel="stylesheet" type="text/css" href="resources/css/register/view.css" media="all">
    <script type="text/javascript" src="resources/js/view.js"></script>
    <script type="text/javascript" src="resources/js/calendar.js"></script>
</head>
<body id="main_body">
<!--<img id="top" src="top.png" alt="">-->
<div id="form_container">

    <h1>
        <p id="form_title"><img src="/appstore-wap/resources/img/appstore-logo.png" alt="icon" class="homeimage">
        <span><fmt:message key="reg.page.create.account"/></span>
        </p>
    </h1>

    <form id="register" class="appnitro register_compact_form" method="post" action="register">
        <div class="form_description">
            <h3><fmt:message key="reg.page.personal.details"/></h3>
            <p id="form_feedback"><c:out value="${msg}"/></p>
        </div>
        <ul>

            <li id="li_1">
                <label class="description" for="fullName"><fmt:message key="reg.page.form.fullName"/> <span class="required">*</span> </label>

                <div>
                    <input id="fullName" name="fullName" class="element text medium" type="text" maxlength="100"
                           value='<c:out value="${fullName}"/>'/>
                </div>
            </li>

            <li id="li_5">
                <label class="description" for="element_5"><fmt:message key="reg.page.form.mobileNo"/> <span
                        class="required">*</span> </label>
		<span>
			<input id="element_5_1" name="phone_1" class="element text" size="3" maxlength="3"
                   value="679" type="text" readonly> -
			<label for="element_5_1">(###)</label>
		</span>
		<span>
			<input id="element_5_2" name="phone_2" class="element text" size="7" maxlength="7" value='<c:out value="${phone_2}"/>' type="text">
			<label for="element_5_2">(#######)</label>
		</span>


            </li>
            <li id="li_4">
                <label class="description" for="element_4"><fmt:message key="reg.page.form.birthDate"/> <span class="required">*</span> </label>
		<span>
			<input id="element_4_1" name="birth_date" class="element text" size="2" maxlength="2"
                   value='<c:out value="${birth_date}"/>' type="text"> /
			<label for="element_4_1">DD</label>
		</span>
		<span>
			<input id="element_4_2" name="birth_month" class="element text" size="2" maxlength="2"
                   value='<c:out value="${birth_month}"/>' type="text"> /
			<label for="element_4_2">MM</label>
		</span>
		<span>
	 		<input id="element_4_3" name="birth_year" class="element text" size="4" maxlength="4"
                   value='<c:out value="${birth_year}"/>' type="text">
			<label for="element_4_3">YYYY</label>
		</span>

		<span id="calendar_4">
			<img id="cal_img_4" class="datepicker" src="resources/css/register/calendar.gif" alt="Pick a date.">
		</span>
                <script type="text/javascript">
                    Calendar.setup({
                        inputField: "element_4_3",
                        baseField: "element_4",
                        displayArea: "calendar_4",
                        button: "cal_img_4",
                        ifFormat: "%B %e, %Y",
                        onSelect: selectEuropeDate
                    });
                </script>

            </li>
            <li id="li_9">
                <label class="description" for="element_9"><fmt:message key="reg.page.form.emailAddr"/><span class="required">*</span> </label>

                <div>
                    <input id="element_9" name="email_address" class="element text medium" type="text" maxlength="255"
                           value='<c:out value="${email_address}"/>'/>
                </div>
            </li>
            <div class="form_description">
                <h3><fmt:message key="reg.page.create.userpwd"/></h3>
            </div>
            <li id="li_6">
                <label class="description" for="element_6"><fmt:message key="reg.page.form.uname"/> <span class="required">*</span> </label>

                <div>
                    <input id="element_6" name="user_name" class="element text medium" type="text" maxlength="255"
                           value='<c:out value="${user_name}"/>'/>
                </div>
            </li>
            <li id="li_7">
                <label class="description" for="element_7"><fmt:message key="reg.page.form.password"/> <span class="required">*</span> </label>

                <div>
                    <input id="element_7" name="password" class="element text medium" type="password" maxlength="255"
                           value=""/>
                </div>
            </li>
            <li id="li_8">
                <label class="description" for="element_8"><fmt:message key="reg.page.form.password.re"/> <span class="required">*</span> </label>

                <div>
                    <input id="element_8" name="password_confirm" class="element text medium" type="password" maxlength="255"
                           value=""/>
                </div>
            </li>
            <div class="form_description">
                <h3><fmt:message key="reg.page.terms.condt.title"/></h3>
            </div>
            <p><fmt:message key="reg.page.terms.condt.desc"/></p>
            <li id="li_10">
            </li>

            <li class="buttons">
                <input id="saveForm" class="button_text" type="submit" name="submit" value="Submit"/>
            </li>
        </ul>
    </form>
    <div id="footer">
        <p><fmt:message key="page.footer"/> <a href="http://www.hsenidmobile.com"><fmt:message key="page.footer1"/></a> </p>
    </div>
</div>
</body>
</html>
</fmt:bundle>