<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<fmt:bundle basename="messages">
    <head>
        <title><fmt:message key="login.title"/></title>
    </head>

    <body>
    <div class="content-login">
        <div class="login-form">
            <c:if test="${not empty error}">
                <div class="alert-error">
                    <fmt:message key="error.login"/>
                </div>
            </c:if>

            <c:if test="${not empty errorMessage}">
                <div class="alert-error">
                    <c:out value="${errorMessage}"/>
                </div>
            </c:if>
            <form action="loginRedirect" method="POST" id="user-registration-form">

                <input name="appId" value="${appId}" hidden="yes" type="hidden"/>

                <fieldset class="username_login-field">
                        <label class="user_login-fields-label"><fmt:message key="app.store.login"/></label><br/>
                        <input type="text" name="username" password="username" placeholder=" Username"/>
                </fieldset>

                <fieldset class="user_login-fields">
                    <label class="user_login-fields-label"><fmt:message key="app.store.password"/></label>
                    <input type="password" name="password" password="password" placeholder=" Password"/>
                </fieldset>

                <div class="enter-wish">
                    <input type="submit" value="Login"/>
                </div>
                <div class="enter-wish">
                    <a href="register" class="register_btn"><fmt:message key="wap.register"/> </a>
                </div>
                <div class="enter-wish">
                    <a href="forgot-pass" class="register_btn"><fmt:message key="wap.forgotpass"/> </a>
                </div>
            </form>
        </div>
    </div>

    </body>
</fmt:bundle>
</html>

