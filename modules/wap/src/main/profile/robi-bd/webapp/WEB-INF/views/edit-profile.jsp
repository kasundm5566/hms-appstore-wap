<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<fmt:bundle basename="messages">
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><fmt:message key="user.edit.page.title"/></title>
    </head>
    <body id="main_body">
    <div id="form_container">
        <form id="edit-profile" class="appnitro register_compact_form" method="post" action="edit-profile">
            <div class="form_description">
                <c:if test="${not empty msg}">
                    <div>
                        <p style="font-weight: bold" class="form_feedback"><fmt:message key="${msg}"/></p>
                    </div>
                </c:if>
                <h3><fmt:message key="reg.page.personal.details"/></h3>
            </div>
            <ul>

                <li id="li_1">
                    <label class="description">
                        <fmt:message key="reg.page.form.firstName"/>
                        <span class="required">*</span>
                    </label>

                    <div>
                        <input id="firstName" name="firstName" class="element text medium" type="text" maxlength="100"
                               value='<c:out value="${formData['firstName']}"/>'/>

                        <p class="form_feedback"><c:out value="${errors['firstName']}"/></p>
                    </div>
                </li>


                <li id="li_9">
                    <label class="description" for="element_9"><fmt:message key="reg.page.form.lastName"/><span
                            class="required">*</span> </label>

                    <div>
                        <input id="element_9" name="lastName" class="element text medium" type="text"
                               maxlength="255"
                               value='<c:out value="${formData['lastName']}"/>'/>

                        <p class="form_feedback"><c:out value="${errors['lastName']}"/></p>
                    </div>
                </li>

                <li class="buttons">
                    <span>
                    <input id="clearForm" class="button_text" type="reset" name="submit"
                           value="<fmt:message key='ver.page.from.btn.cancel'/>"/>
                    </span>
                    <span>
                    <input id="saveForm" class="button_text" type="submit" name="submit"
                           value="<fmt:message key='ver.page.from.btn.submit'/>"/>
                    </span>


                </li>
            </ul>
        </form>

    </div>
    </body>
    </html>
</fmt:bundle>