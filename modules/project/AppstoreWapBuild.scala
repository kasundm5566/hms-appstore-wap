import sbt._
import Keys._
import repositories._
import com.github.siasia.PluginKeys._
import com.github.siasia.WebPlugin._
import sbt.ExclusionRule
import sbt.ScalaVersion
import SbtProfilePlugin._
import SbtProfilePlugin.SbtProfileKeys._

object AppstoreWapBuild extends Build {
  val ProjectVersion  = "1.0.30"
  val Organization    = "hms.appstore.wap"

  val ProjectProfile  = "robi-bd"
  val ProjectProfiles = Seq("dialog", "vodafone", "robi-bd")

  val discoveryApiVersion    = "1.0.65-SNAPSHOT"
  val ScalaVersion           = "2.10.2"
  val SpringVersion          = "3.2.3.RELEASE"
  val JettyVersion           = "8.1.13.v20130916"

  val DiscoveryClient                   = "hms.appstore.api"                %%  "discovery-client"          % discoveryApiVersion
  val HttpServletComponents             = "hms.scala.http"                  %% "http-servlet-components"    % "1.0.0"


  val CommonsCodec                      = "commons-codec"                   %   "commons-codec"             % "1.5"

  val SpringCore                        = "org.springframework"             %   "spring-core"               % SpringVersion
  val SpringExpression                  = "org.springframework"             %   "spring-expression"         % SpringVersion
  val SpringBeans                       = "org.springframework"             %   "spring-beans"              % SpringVersion
  val SpringAop                         = "org.springframework"             %   "spring-aop"                % SpringVersion
  val SpringContext                     = "org.springframework"             %   "spring-context"            % SpringVersion
  val SpringContextSupport              = "org.springframework"             %   "spring-context-support"    % SpringVersion
  val SpringOxm                         = "org.springframework"             %   "spring-oxm"                % SpringVersion
  val SpringWeb                         = "org.springframework"             %   "spring-web"                % SpringVersion
  val SpringMvc                         = "org.springframework"             %   "spring-webmvc"             % SpringVersion
  val SpringTest                        = "org.springframework"             %   "spring-test"               % SpringVersion

  val ServletApi                        = "javax.servlet"                   %   "javax.servlet-api"         % "3.0.1"         % "provided->default"
  val Jstl                              = "jstl"                            %   "jstl"                      % "1.2"
  val SiteMesh                          = "org.sitemesh"                    %   "sitemesh"                  % "3.0-alpha-2"

  val JodaTime                          = "joda-time"                       % "joda-time"                   % "2.1"
  val JodaTimeTags                      = "joda-time"                       % "joda-time-jsptags"           % "1.1.1"
  val JodaConvert                       = "org.joda"                        % "joda-convert"                % "1.2"

  val JettyWebAppContainer              = "org.eclipse.jetty"               %   "jetty-webapp"              % JettyVersion    % "container"
  val JettyJspContainer                 = "org.eclipse.jetty"               %   "jetty-jsp"                 % JettyVersion    % "container" excludeAll(ExclusionRule(organization = "org.slf4j"))

  val TypesafeConfig                    = "com.typesafe"                    %   "config"                    % "1.0.0"
  val TypesafeLogging                   = "com.typesafe"                    %%  "scalalogging-slf4j"        % "1.0.1"
  val HmsCommonUtil                     = "hms.common"                      %   "hms-common-util"           % "1.0.6"
  val userAgentUtils                    = "eu.bitwalker"                    %   "UserAgentUtils"            % "1.14"

  val jettyConf = config("container")

  val jettyPluginSettings = Seq(
    libraryDependencies ++= Seq(
      JettyWebAppContainer,
      JettyJspContainer
    ),
    port in jettyConf := 8081
  )

  val appstoreWapDependencies = Seq(
    SpringCore,
    SpringExpression,
    SpringBeans,
    SpringAop,
    SpringContext,
    SpringContextSupport,
    SpringOxm,
    SpringWeb,
    SpringMvc,
    SpringTest,
    DiscoveryClient,
    Jstl,
    ServletApi,
    TypesafeLogging,
    TypesafeConfig,
    SiteMesh,
    HttpServletComponents,
    HmsCommonUtil,
    CommonsCodec,
    JodaTime,
    JodaConvert,
    JodaTimeTags,
    userAgentUtils
  )

  val testDependencies = Seq(
    "org.scalamock"         %% "scalamock-specs2-support"     % "3.0.1"         % "test",
    "hms.specs"             %% "specs-matchers"               % "0.1.0"         % "test"
  )

  val IvyCredentialFile = if (ProjectVersion.endsWith("SNAPSHOT")) {
    Path.userHome / ".ivy2" / ".credentials-snapshot"
  } else {
    Path.userHome / ".ivy2" / ".credentials-release"
  }

  val moduleLookupConfigurations = DefaultResolver.moduleConfig

  val excludedFilesInJar: NameFilter = (s: String) => """(.*?)\.(properties|props|conf|dsl|txt|xml)$""".r.pattern.matcher(s).matches

  lazy val baseSettings = {
    Defaults.defaultSettings ++ profileSettings ++ Seq(
      version := ProjectVersion,
      organization := Organization,
      scalaVersion := ScalaVersion,
      scalacOptions += "-deprecation",
      scalacOptions += "-unchecked",
      moduleConfigurations ++= moduleLookupConfigurations,
      publishTo <<= (version) {
        version: String =>
          val repo = "http://archiva.hsenidmobile.com/repository/"
          if (version.trim.endsWith("SNAPSHOT"))
            Some("Archiva Managed snapshots Repository" at repo + "snapshots/")
          else
            Some("Archiva Managed internal Repository" at repo + "internal/")
      },
      credentials += Credentials(Path.userHome / ".ivy2" / ".credentials-snapshot"),
      logBuffered := false,
      parallelExecution in Test := false,
      buildProfile := ProjectProfile,
      buildProfiles := ProjectProfiles,
      offline := true
    )
  }

  lazy val modules = Project(id = "appstore-wap", base = file("."),
    settings = baseSettings ++ Seq(
      name := "appstore-wap"
    )
  ) aggregate (appstore)


  lazy val appstore = Project(id = "wap", base = file("wap"),
    settings = baseSettings ++ webSettings ++ jettyPluginSettings ++ Seq(
      name := "appstore",
      artifactName := {
        (config: ScalaVersion, module: ModuleID, artifact: Artifact) => "appstore-wap" + "." + "war"
      },
      libraryDependencies ++= appstoreWapDependencies,
      libraryDependencies ++= testDependencies ,
      webappResources in Compile <<= (webappResources in Compile, baseDirectory, buildProfile) {
        (wrs, bd, p) => {
          val resourceDir = bd / "src" / "main" / "profile" / p / "webapp"
          if (resourceDir.exists) (wrs ++ Seq(resourceDir)).reverse else wrs
        }
      }
    )
  )
}
