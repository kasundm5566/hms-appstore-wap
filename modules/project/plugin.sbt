import sbt._
import Keys._

resolvers += "hms internal" at "http://archiva.hsenidmobile.com/repository/internal"

addSbtPlugin("com.github.mpeltonen" % "sbt-idea" % "1.3.0")

addSbtPlugin("hms.sbt.plugin" % "sbt-profile-plugin" % "0.1.3")

libraryDependencies += "com.github.siasia" %% "xsbt-web-plugin" % "0.12.0-0.2.11.1"



